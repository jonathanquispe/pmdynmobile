/**
 * Module to create eTag in index.json 
 * Autor: Henry Jonathan Quispe
 */

var createEtagDate = function (date){
	var	response;
	response = date.getFullYear().toString()+
			   date.getMonth().toString()+
			   date.getDay().toString()+
			   date.getHours().toString()+
			   date.getMinutes().toString()+
			   date.getSeconds().toString()+
			   date.getMilliseconds().toString();
	return response;
};


var createDateExpirationDays = function (date, addDays){
	var time,
		response,
		miliseconds;

	time =date.getTime();
	miliseconds = addDays*24*60*60*1000;
	date.setTime(time+miliseconds);
	return date;
};


var getFormatDate = function (date){
	var response;
	response  = date.toLocaleString();
	return response; 
};


var verifyLastModified = function (name,last,file,mode){
	var i,
		currentInfo;
	for (i=0; i< file[mode].length ;i++){
		currentInfo= file[mode][i];				
		if(currentInfo.file == name && currentInfo.lastModified == last){			
			return {"etag":currentInfo.etag, "lastModified":currentInfo.lastModified};
		}
	}	
	return false;		 
};


var verifyFile = function (name,file,mode){
	var i,
		currentInfo;
	for (i=0; i< file[mode].length ;i++){
		currentInfo= file[mode][i];				
		if(currentInfo.file == name){			
			return true;
		}
	}	
	return false;		 
};




var fs = require('fs'),
    configFile,
	oldFile,
	newFile,
	jsonFile,
	arrayFiles=[],
	readFile,
	isModified,
	files,
	currentFile,
	expirationDays, 
	dataCurrent = new Date(),
	etag,
	currentDate = new Date(),
	dateExpiration=null,
	i, 
	responseData={
		//"build-dev":null,
		"build-prod":null,
	};

	jsonFile = fs.readFileSync('config/etag.json', 'utf8');
	oldFile = fs.readFileSync('index.json', 'utf8');	
	jsonFile = JSON.parse(jsonFile);	
	oldFile = JSON.parse(oldFile);


	//BUILD-DEV
	/*expirationDays = jsonFile["build-dev"].expirationDays;	
	files = jsonFile["build-dev"].files;
	etag= createEtagDate(currentDate);
	if(typeof expirationDays === "number"){
		dateExpiration= createDateExpirationDays(currentDate,expirationDays);
		console.log(dateExpiration);
		currentDate = dateExpiration;
	}

	for (i=0; i< files.length ;i++){		
		dataCurrent = {};
		currentFile = files[i];
		dataCurrent["file"]= currentFile;
		if(!verifyFile(currentFile,oldFile,"build-dev")){
			console.log(currentFile);

			
			jsonOldFile = fs.statSync(currentFile);
			dataCurrent["lastModified"]= jsonOldFile.mtime.toUTCString();
			dataCurrent["etag"] = etag;	
			arrayFiles.push(dataCurrent);

		}else{

			if(dateExpiration != null){
				dataCurrent["expiration"]=getFormatDate(dateExpiration);
			}
			jsonOldFile = fs.statSync(currentFile);				   

		    isModified = verifyLastModified(currentFile,jsonOldFile.mtime.toUTCString(),oldFile,"build-dev");		
			if(isModified){
				dataCurrent["lastModified"]= isModified.lastModified;
				dataCurrent["etag"]= isModified.etag;
			}else{			
				dataCurrent["lastModified"]= jsonOldFile.mtime.toUTCString();
				dataCurrent["etag"] = etag;			
			}
			arrayFiles.push(dataCurrent);
		}
	}
	responseData["build-dev"]=arrayFiles;	*/


	//BUILD-PROD

	arrayFiles= [];
	expirationDays = jsonFile["build-prod"].expirationDays;	
	files = jsonFile["build-prod"].files;	
	etag= createEtagDate(currentDate);
	if(typeof expirationDays === "number"){
		dateExpiration= createDateExpirationDays(currentDate,expirationDays);		
		currentDate = dateExpiration;
	}

	for (i=0; i< files.length ;i++){		
		dataCurrent = {};
		currentFile = files[i];
		dataCurrent["file"]= currentFile;

		if(!verifyFile(currentFile,oldFile,"build-prod")){			
			jsonOldFile = fs.statSync(currentFile);
			dataCurrent["lastModified"]= jsonOldFile.mtime.toUTCString();
			dataCurrent["etag"] = etag;	
			arrayFiles.push(dataCurrent);

		}else{

			if(dateExpiration != null){
				dataCurrent["expiration"]=getFormatDate(dateExpiration);
			}

			jsonOldFile = fs.statSync(currentFile);				   			
		    isModified = verifyLastModified(currentFile,jsonOldFile.mtime.toUTCString(),oldFile,"build-prod");		
			if(isModified){
				dataCurrent["lastModified"]= isModified.lastModified;
				dataCurrent["etag"]= isModified.etag;
			}else{			
				dataCurrent["lastModified"]= jsonOldFile.mtime.toUTCString();
				dataCurrent["etag"] = etag;			
			}
			arrayFiles.push(dataCurrent);
		}
	}
	responseData["build-prod"]=arrayFiles;	
	fs.writeFile('index.json', JSON.stringify(responseData), function (err) {
	  	if (err) throw err;
	  	console.log('It\'s saved!');
	});

