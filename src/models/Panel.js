(function(){
	var PanelModel = Backbone.Model.extend({
		defaults: {
			items: [],
			mode: "edit",
			namespace: "pmdynaform",
			id: PMDynaform.core.Utils.generateID(),
 			name: PMDynaform.core.Utils.generateName("form"),
			type: "form"
		},
		getData: function() {
			return {
				type: this.get("type"),
				name: this.get("name"),
				variables: {}
			}
		}
	});
	PMDynaform.extendNamespace("PMDynaform.model.Panel", PanelModel);

}());