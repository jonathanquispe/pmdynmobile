(function(){
	var ButtonModel = Backbone.Model.extend({
		defaults: {
			colSpan: 12,
			disabled: false,
            namespace: "pmdynaform",
			id: PMDynaform.core.Utils.generateID(),
 			name: PMDynaform.core.Utils.generateName("button"),
			label: "untitled label",
			type: "button"
		}
	});
	
	PMDynaform.extendNamespace("PMDynaform.model.Button", ButtonModel);
}());