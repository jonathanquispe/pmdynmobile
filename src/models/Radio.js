(function(){
	var RadioboxModel =  PMDynaform.model.Field.extend({
		defaults: {
			colSpan: 12,
			colSpanLabel: 3,
            colSpanControl: 9,
            namespace: "pmdynaform",
			id: PMDynaform.core.Utils.generateID(),
 			name: PMDynaform.core.Utils.generateName("radio"),
			dataType: "string",
            dependenciesField: [],
            disabled: false,
            defaultValue: "",
            label: "",
            localOptions: [],
            group: "form",
            hint: "",
            options: [
            	{
                    label : "empty",
                    value: "empty"
                }
            ],
            mode: "edit",
            type: "radio",
            readonly: false,
            remoteOptions: [],
            required: false,
            validator: null,
            valid: true,
            variable: null,
            var_uid: null,
            var_name: null,
            variableInfo: {},
            value: "",
            columnName : null,
            originalType : null,
            data : null,
            itemClicked : false
		},
		initialize: function(attrs) {
			var data;
			this.set("label", this.checkHTMLtags(this.get("label")));
            this.on("change:label", this.onChangeLabel, this);
            this.on("change:value", this.onChangeValue, this);
            this.on("change:options", this.onChangeOptions, this);
			this.set("validator", new PMDynaform.model.Validator({
				domain: true
			}));
			this.set("dependenciesField",[]);
			this.verifyControl();
			this.initControl();
			this.setLocalOptions();
            data = this.get("data");
            if ( data ) {
                this.set("value",data["value"]);
            } else {
                if (this.get("options").length){
                    this.set("value",this.get("options")[0]["value"]);
                    this.set("data",{
                        value:this.get("options")[0]["value"],
                        label : this.get("options")[0]["label"]
                    });
                } else {
                    this.set("data",{value:"", label:""});
                    this.set("value","");
                }
            }

            if (this.get("group") === "form"){
				if(this.get("var_name").trim().length === 0){
					this.attributes.name = this.get("id");
				}
            }else{
                this.attributes.name = this.get("id");
            }
			//this.reviewRemoteVariable();
            
		},
		initControl: function() {
			var opts = this.get("options"), 
			i,
			newOpts = [],
			itemsSelected = [];

			if (this.get("defaultValue")) {
                this.set("value", this.get("defaultValue"));
            }
			for (i=0; i<opts.length; i+=1) {
				if (!opts[i].label) {
					throw new Error ("The label parameter is necessary for the field");
				}
				if (!opts[i].value) {
					opts[i].value = opts[i].label;
				}
				if(opts[i].selected) {
					itemsSelected.push(opts[i].value.toString());
				}
				newOpts.push({
                    label: this.checkHTMLtags(opts[i].label),
                    value: this.checkHTMLtags(opts[i].value),
                    selected: opts[i]? opts[i]: false
                });
			}
                
            this.set("options", newOpts);
			this.set("selected", itemsSelected);
		},
		setLocalOptions: function () {
            this.set("localOptions", this.get("options"));
            return this;
        },
		isValid: function() {
            this.set("valid", this.get("validator").get("valid"));
        	return this.get("valid");
        },
        verifyControl: function() {
			var opts = this.get("options"), i;
			for (i=0; i<opts.length; i+=1) {
				if (!opts[i].label) {
					throw new Error ("The label parameter is necessary for the field");
				}

				if (!opts[i].value && ( typeof opts[i].value !== "number") ) {
					opts[i].value = opts[i].label;
				} else {
					opts[i].value = opts[i].value.toString();
				}
			}
			this.set("value", this.get("value").toString());
		},
		validate: function(attrs) {
			
            this.get("validator").set("type", attrs.type);
            this.get("validator").set("value", attrs.value.length);
            this.get("validator").set("valueDomain", attrs.value);
            this.get("validator").set("required", attrs.required);
            this.get("validator").set("dataType", attrs.dataType);
            this.get("validator").verifyValue();
            this.isValid();
            return this.get("valid");
		},
		setItemClicked: function(itemUpdated) {
			var opts = this.get("options"),
				selected = this.get("selected"),
				position,
				newSelected,
				i;
            this.itemClicked = true;
			if (opts) {
				for(i=0; i< opts.length; i+=1) {
					if(opts[i].value.toString() === itemUpdated.value.toString()) {
						this.set("value", itemUpdated.value.toString());
					}
				}
			}
			return this;
		},
		getData: function() {

            //console.log("getData text")
            /*return {
                name: this.get("variable") ? this.get("variable").var_name : this.get("name"),
                value: this.get("value")
            };*/
            if (this.get("group") == "grid"){
                return {
                    name : this.get("columnName") ? this.get("columnName"): "",
                    value :  this.get("value")
                }

            } else {
                return {
                    name : this.get("name") ? this.get("name") : "",
                    value :  this.get("value")
                }
            }
		},
        onChangeValue: function (attrs, options) {
            var i, opts, data = {};
            this.attributes.value = this.checkHTMLtags(attrs.attributes.value);
            if (this.attributes.options) {
                this.get("validator").set({
                    valueDomain: this.get("value"),
                    options: this.get("options") || []
                });
                this.get("validator").verifyValue();
            }
            if (!this.itemClicked){
                opts = this.get("options");
                for ( i = 0 ; i < opts.length ; i+=1 ) {
                    if (opts[i]["value"] === this.get("value")){
                        data["value"] = opts[i]["value"];
                        data["label"] = opts[i]["label"];
                        break;
                    }
                }
                this.attributes.data = data;
            }
            this.itemClicked = false;
            return this;
        }        
	});
	PMDynaform.extendNamespace("PMDynaform.model.Radio",RadioboxModel);
}());