(function(){
	var HiddenModel = PMDynaform.model.Field.extend({
		defaults: {
			colSpan: 12,
			dataType: "string",
			namespace: "pmdynaform",
			defaultValue: "",
			id: PMDynaform.core.Utils.generateID(),
 			name: PMDynaform.core.Utils.generateName("hidden"),
			type: "hidden",
			valid: true,
			value: "",
			group : "form",
			var_name : "",
			data : null
		},
		initialize: function (options) {
			var data;
            this.set("defaultValue", this.checkHTMLtags(this.get("defaultValue")));
            this.on("change:value", this.onChangeValue, this);
			data = this.get("data");
			if ( data ) {
				this.set("value", data["value"]);
			} else {
				this.set("data",{value:"", label:""});
				this.set("value","");
			}
			this.initControl();

			if ( this.get("var_name").trim().length === 0) {
				if ( this.get("group") === "form" ) {
                	this.attributes.name = "";
				} else {
            		this.attributes.name = this.get("id");
				}
			}
			return this;
		},
		initControl: function () {
			if (this.get("defaultValue")) {
                this.set("value", this.get("defaultValue"));
			}
		},
		checkHTMLtags: function (value) {
            var i,
            newValue = value;
            if (typeof value === "string") {
            	if (value.match(/([\<])([^\>]{1,})*([\>])/i) !== null) {
	                value = value.replace(/</g, "&lt;");
	                newValue = value.replace(/>/g, "&gt;");
	            }
	            if (/\"|\'/g.test(value)) {
	                newValue = newValue.replace(/"/g, "&quot;");
	                newValue = newValue.replace(/'/g, "&#39;");
	            }
            }

            return newValue;
        },
		onChangeValue: function () {
		}
	});
	
	PMDynaform.extendNamespace("PMDynaform.model.Hidden", HiddenModel);
}());