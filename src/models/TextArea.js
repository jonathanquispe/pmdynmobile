(function(){
	var TextAreaModel =  PMDynaform.model.Field.extend({
		defaults: {
			type: "text",
			placeholder: "untitled",
			label: "untitled label",
			id: PMDynaform.core.Utils.generateID(),
            name: PMDynaform.core.Utils.generateName("textarea"),
			colSpan: 12,
            value: "",
            defaultValue: "",
            colSpanLabel: 3,
            colSpanControl: 9,
            namespace: "pmdynaform",
            maxLengthLabel: 15,
            rows: 2,
            group: "form",
            dataType: "string",
            hint: "",
            disabled: false,
            maxLength: null,
            mode: "edit",
            required: false,
            validator: null,
            valid: true,
            columnName : null,
            originalType : null,
            options : [],
            data : null,
            localOptions: [],
            remoteOptions: []
        },
        getData: function() {
            if (this.get("group") == "grid"){
                return {
                    name : this.get("columnName") ? this.get("columnName"): "",
                    value :  this.get("value")
                }

            } else {
                return {
                    name : this.get("name") ? this.get("name") : "",
                    value :  this.get("value")
                }
            }
        },
        initialize: function(attrs) {
            var data, maxLength;
            this.set("label", this.checkHTMLtags(this.get("label")));
            this.set("defaultValue", this.checkHTMLtags(this.get("defaultValue")));

            this.on("change:label", this.onChangeLabel, this);
            this.on("change:value", this.onChangeValue, this);
            
            this.set("validator", new PMDynaform.model.Validator({
                "type"  : this.get("type"),
                "required" : this.get("required"),
                "maxLength" : this.get("maxLength"),
                "dataType" : this.get("dataType") || "string",
                "regExp" : { 
                    validate : this.get("validate"), 
                    message : this.get("validateMessage")
                }
            }));

            data = this.get("data");
            if ( data ) {
                this.set("value", data["label"]);
            } else {
                this.set("data",{value:"", label:""});
                this.set("value","");
            }
            this.initControl();

			if ( this.get("var_name").trim().length === 0) {
				if ( this.get("group") === "form" ) {
                	this.attributes.name = "";
				} else {
            		this.attributes.name = this.get("id");
				}
			}
        },
        initControl: function() {
            if (this.get("defaultValue")) {
                this.set("value", this.get("defaultValue"));
            }
        },
        isValid: function(){
            this.set("valid", this.get("validator").get("valid"));
            return this.get("valid");
        },
        validate: function (attrs) {
            var valueFixed = attrs.value.trim();
            this.set("value", valueFixed);
            this.get("validator").set("value", valueFixed);
            this.get("validator").verifyValue();
            this.isValid();
            return this.get("valid");
        }
    });
    PMDynaform.extendNamespace("PMDynaform.model.TextArea", TextAreaModel);
}());