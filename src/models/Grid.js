(function(){
	var GridModel = PMDynaform.model.Field.extend({
		defaults: {
			title: "Grid",
			colSpan: 12,
			colSpanLabel: 3,
            colSpanControl: 9,
            namespace: "pmdynaform",
			columns: [],
			data: [],
			disabled: false,
			id: PMDynaform.core.Utils.generateID(),
 			name: PMDynaform.core.Utils.generateName("grid"),
			gridtable: [],
			layoutOpt: [
				"responsive",
				"static",
				"form"
			],
			layout: "responsive",
			pager: true,
			paginationItems: 1,
			pageSize: 5,
			mode: "edit",
			rows: 1,
			type: "grid",
			functions: false,
			totalrow: [],
			functionOptions: {
				"sum": "sumValues",
				"avg": "avgValues"
			},
			dataColumns: [],
			gridFunctions: [],
			titleHeader: [],
			valid : true,
			countHiddenControl : 0,
			newRow : true,
			deleteRow : true
		},
		initialize: function (options) {
			var pagesize;
			if (options["addRow"] === undefined){
				this.set("addRow",true);
			}
			if(options["deleteRow"] === undefined){
				this.set("deleteRow",true);	
			}
			if ( jQuery.isNumeric(this.get("pageSize") ) ){
				pagesize = parseInt(this.get("pageSize"),10);
				if ( pagesize < 1 ) {
					pagesize = 1;
					this.set("pager",false);
				}
			} else {
				this.set("pager",false);
			}
			this.set("pageSize", pagesize);
			this.set("label", this.checkHTMLtags(this.get("label")));
			this.on("change:label", this.onChangeLabel, this);
			if(options.project) {
                this.project = options.project;
            }
            this.fixCoutFieldsHidden();
            this.setLayoutGrid();
            this.setPaginationItems();
            this.checkTotalRow();
		},
		setLayoutGrid: function () {
			if ($.inArray(this.get("layout"), this.get("layoutOpt")) < 0) {
				this.set("layout", "responsive");
			}

			return this;
		},
		setPaginationItems: function () {
			var rows = this.get("rows"),
			size = this.get("pageSize"),
			pagerItems;

			pagerItems = Math.ceil(rows/size) ? Math.ceil(rows/size) : 1;

			this.set("paginationItems", pagerItems);

			return this;
		},
		checkTotalRow: function () {
			var i;

			loop_total:
			for (i=0; i<this.attributes.columns.length; i+=1) {
				if(this.attributes.columns[i].operation) {
					if(this.attributes.functionOptions[this.attributes.columns[i].operation.toLowerCase()]) {
						this.attributes.functions = true;
						break loop_total;
					}

				}
			}
			return this;
		},
		applyFunction: function () {
			var i;

			for (i=0; i<this.attributes.columns.length; i+=1) {
				if(this.attributes.columns[i].operation) {
					if(this.attributes.functionOptions[this.attributes.columns[i].operation.toLowerCase()]) {
						this.attributes.totalrow[i] = this[this.attributes.functionOptions[this.attributes.columns[i].operation.toLowerCase()]](i);
					}
				}
			}

			return this;
		},
		sumValues: function (colIndex) {
			var i,
			sum = 0,
			grid = this.attributes.gridFunctions;

			for (i=0; i<grid.length; i+=1) {
				sum += grid[i][colIndex];
			}

			return sum;
		},
		avgValues: function (colIndex) {
			var i,
			sum = 0,
			grid = this.attributes.gridFunctions;

			for (i=0; i<grid.length; i+=1) {
				sum += grid[i][colIndex];
			}
			
			return Math.round ((sum/grid.length) * 100 ) /100 ;
		},
		getData: function () {
			return {
				type: this.get("type"),
				name: this.get("name"),
				gridtable: null
			}
            return formData;
			
		},
		fixCoutFieldsHidden : function(){
        	var i, countHiddenControl = 0;
        	for ( i = 0 ; i < this.get("columns").length ; i +=1) {
        		if (this.get("columns")[i].type === "hidden"){
        			countHiddenControl +=1;
        		}
        	}
        	this.set("countHiddenControl",countHiddenControl);
        	return this;
        }	
	});
	
	PMDynaform.extendNamespace("PMDynaform.model.GridPanel", GridModel);
}());