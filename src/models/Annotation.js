(function(){
	var Annotation = PMDynaform.model.Field.extend({
		defaults: {
            type: "annotation",
            label: "untitled label",
            id: PMDynaform.core.Utils.generateID(),
            name: PMDynaform.core.Utils.generateName("title"),
            colSpan: 12,
            namespace: "pmdynaform"
        },
        initialize: function() {
            this.set("label", this.checkHTMLtags(this.get("label")));
            this.on("change:label", this.onChangeLabel, this);
        }
    });
    PMDynaform.extendNamespace("PMDynaform.model.Annotation", Annotation);
}());