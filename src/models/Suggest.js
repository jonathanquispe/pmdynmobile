(function(){

	var SuggestModel = PMDynaform.model.Field.extend({
		defaults: {
            autoComplete: "off",
            type: "text",
            placeholder: "untitled",
            label: "untitled label",
            id: PMDynaform.core.Utils.generateID(),
            name: PMDynaform.core.Utils.generateName("suggest"),
            colSpan: 12,
            colSpanLabel: 3,
            colSpanControl: 9,
            namespace: "pmdynaform",
            value: "",
            group: "form",
            defaultValue: "",            
            maxLengthLabel: 15,
            mode: "edit",
            tooltipLabel: "",
            disabled: false,
            dataType: "string",
            executeInit: true,
            required: false,
            maxLength: null,
            validator: null,
            valid: true,
            proxy: null,
            variable: null,
            var_uid: null,
            var_name: null,
            options:[],
            localOptions: [],
            remoteOptions: [],
            dependenciesField: [],
            columnName : null,
            originalType : null,
            mask : "",
            clickedControl : true
        },
        initialize: function(attrs) {
            var data;
            this.set("dataType", this.get("dataType").trim().length?this.get("dataType"):"string");
            this.set("dependenciesField",[]);
            this.set("label", this.checkHTMLtags(this.get("label")));
            this.set("defaultValue", this.checkHTMLtags(this.get("defaultValue")));
            this.on("change:label", this.onChangeLabel, this);
            this.on("change:value", this.onChangeValue, this);
            this.set("validator", new PMDynaform.model.Validator());
            this.initControl();
            this.setLocalOptions();
            data = this.get("data");
            if ( data ) {
                this.set("value",data["value"]);
            } else {
                this.set("data",{value:"", label:""});
                this.set("value","");
            }
			if ( this.get("var_name").trim().length === 0) {
				if ( this.get("group") === "form" ) {
                	this.attributes.name = "";
				} else {
            		this.attributes.name = this.get("id");
				}
			}
        },
        initControl: function() {
            if (this.get("defaultValue")) {
                this.set("value", this.get("defaultValue"));
            }
        },
        setLocalOptions: function () {
            this.set("localOptions", this.get("options"));
            return this;
        },
        isValid: function(){
            this.set("valid", this.get("validator").get("valid"));
            return this.get("valid");
        },
        emptyValue: function (){
            this.set("value","");
        },
        setDependencies: function(newDependencie) {
            var arrayDep,i, result, newArray = [];
            arrayDep = this.get("dependenciesField");
            if(arrayDep.indexOf(newDependencie) == -1){
                arrayDep.push(newDependencie);
            }
            this.set("dependenciesField",[]);
            this.set("dependenciesField",arrayDep);
        },
        validate: function (attrs) {
            var valueFixed = attrs.value.trim();
            
            this.set("value", valueFixed);
            this.get("validator").set("type", attrs.type);
            this.get("validator").set("required", attrs.required);
            this.get("validator").set("value", valueFixed);
            this.get("validator").set("dataType", attrs.dataType);
            this.get("validator").verifyValue();
            this.isValid();
            return this.get("valid");
        },
        getData: function() {
            //console.log("getData text")
            /*return {
                name: this.get("variable") ? this.get("variable").var_name : this.get("name"),
                value: this.get("value")
            };*/
            if (this.get("group") == "grid"){
                return {
                    name : this.get("columnName") ? this.get("columnName"): "",
                    value :  this.get("value")
                }

            } else {
                return {
                    name : this.get("name") ? this.get("name") : "",
                    value :  this.get("value")
                }
            }
        },
        onChangeValue: function (attrs, options) {
            var data = {}, opts,i, exist = false;
            this.attributes.value = this.checkHTMLtags(attrs.attributes.value);
            opts = this.get("options");
            /*if (this.attributes.options) {
                this.get("validator").set({
                    valueDomain: this.get("value"),
                    options: this.get("options") || []
                });
                this.get("validator").verifyValue();
            }*/
            if ( !this.get("clickedControl") ) {
                if (this.get("data")){
                    for ( i = 0 ; i < opts.length ; i+=1 ) {
                        if ( opts[i]["value"] === this.get("value") ) {
                            data["value"] = opts[i]["value"];
                            data["label"] = opts[i]["label"];
                            this.attributes.data = data;
                            exist = true;
                            //this.attributes.value = opts[i]["label"];
                            break;
                        }
                    }
                    if (!exist) {
                        data["value"] = "";
                        data["label"] = "";
                        //console.log("entra?");
                        this.attributes.data = data;
                    }
                }
            }
            return this;
        }
    });
    PMDynaform.extendNamespace("PMDynaform.model.Suggest", SuggestModel);
}());