(function(){
	var FieldModel = Backbone.Model.extend({
		defaults: {
			colSpan: 12,
            id: PMDynaform.core.Utils.generateID(),
			label: "Untitled",
            name: PMDynaform.core.Utils.generateName(),
			value: "",
            nameGridColum : null
		},
        initialize: function (options) {
            this.set("label", this.checkHTMLtags(this.get("label")));
            this.set("defaultValue", this.checkHTMLtags(this.get("defaultValue")));
        },
		getData: function() {
            /*return {
                name: this.get("variable") ? this.get("variable").var_name : this.get("name"),
                value: this.get("value")
            };*/
            return {
                name : this.get("name") ? this.get("name") : "",
                value :  this.get("value")
            }
        },
        parseLabel: function () {
            var currentLabel = this.get("label"),
                maxLength = this.get("maxLengthLabel"),
                currentSize,
                itemsLabel, 
                k, 
                parsed = false;

            itemsLabel = currentLabel.split(/\s/g);
            for (k=0; k<itemsLabel.length; k+=1) {
                if (itemsLabel[k].length > maxLength) {
                    parsed = true;
                }
            }
            if (parsed) {
                this.set("tooltipLabel", currentLabel);
                this.set("label", currentLabel.substr(0, maxLength-4)+"...");
            }
            return this;
        },
        checkHTMLtags: function (value) {
            var i,
            newValue = value;

            if (typeof value === "string") {
                if (value.match(/([\<])([^\>]{1,})*([\>])/i) !== null) {
                    value = value.replace(/</g, "&lt;");
                    newValue = value.replace(/>/g, "&gt;");
                }
                if (/\"|\'/g.test(newValue)) {
                    newValue = newValue.replace(/"/g, "&quot;");
                    newValue = newValue.replace(/'/g, "&#39;");
                }
            }
            return newValue;
        },
        validate: function (attrs) {
            this.set("value", this.checkHTMLtags(attrs.value));
            this.set("label", this.checkHTMLtags(attrs.label));

            return this;
        },
        getEndpointVariable: function (urlObj) {
        	var prj = this.get("project"),
        	endPointFixed,
        	variable,
        	endpoint;

        	if (prj.endPointsPath[urlObj.type]) {
        		endpoint = prj.endPointsPath[urlObj.type]
        		for (variable in urlObj.keys) {
        			if (urlObj.keys.hasOwnProperty(variable)) {
        				endPointFixed =endpoint.replace(new RegExp(variable, "g"), urlObj.keys[variable]);	
        			}
        		}
        	}

        	return endPointFixed;
        },
        onChangeLabel: function (attrs, options) {
            this.attributes.label = this.checkHTMLtags(attrs.attributes.label);
            
            return this;
        },
        onChangeValue: function (attrs, options) {
            var data = {};
            this.attributes.value = this.checkHTMLtags(attrs.attributes.value);
            
            if (this.attributes.options) {
                this.get("validator").set({
                    valueDomain: this.get("value"),
                    options: this.get("options") || []
                });
                this.get("validator").verifyValue();
            }
            if (this.get("data")){
                data["value"] = this.get("value");
                data["label"] = this.get("value");
                this.attributes.data = data;
            }
            return this;
        },
        onChangeOptions: function () {
            var i,
            newOptions = [],
            options = this.get("options");

            for (i=0; i<options.length; i+=1) {
                newOptions.push(this.checkHTMLtags(options[i]));
            }
            this.attributes.options = newOptions;
            
            return this;
        },
        /**
         * The method check all the fields related to the current field based of the variable and 
         * set the same value to others. After set the value, all the fields are rendered.
         */
        changeValuesFieldsRelated: function () {
            var i,
            currentValue = this.get("value"),
            fieldsRelated = this.get("fieldsRelated") || [];

            for (i=0; i<fieldsRelated.length; i+=1) {
                fieldsRelated[i].model.attributes.value = currentValue;
                fieldsRelated[i].model.get("validator").set({
                    valueDomain: this.get("value"),
                    options: fieldsRelated[i].model.attributes.options || [],
                    domain: true
                });
                fieldsRelated[i].model.get("validator").verifyValue();
                //fieldsRelated[i].model.set("value", currentValue);
                fieldsRelated[i].render();
            }

            return this;
        }
	});
	PMDynaform.extendNamespace("PMDynaform.model.Field", FieldModel);
}());