(function(){
	var SubmitModel =  PMDynaform.model.Field.extend({
		defaults: {
			type: "submit",
			namespace: "pmdynaform",
			placeholder: "untitled",
			label: "untitled label",
			id: PMDynaform.core.Utils.generateID(),
 			name: PMDynaform.core.Utils.generateName("submit"),
			disabled: false,
			colSpan: 12
		}
	});
	PMDynaform.extendNamespace("PMDynaform.model.Submit", SubmitModel);
}()); 