(function(){
	var File =  PMDynaform.model.Field.extend({
		defaults: {
            autoUpload: false,
			camera: true,
            colSpan: 12,
            colSpanLabel: 3,
            colSpanControl: 9,
            namespace: "pmdynaform",
            defaultValue: "",
            disabled: false,
            dnd: false,
            dndMessage: "Drag or choose local files",
            extensions: "pdf, png, jpg, mp3, doc, txt",
            group: "form",
            height: "200px",
            hint: "",
			id: PMDynaform.core.Utils.generateID(),
            items: [],
            label: "Untitled label",
            labelButton: "Choose Files",
            mode: "edit",
            multiple: false,
            name: PMDynaform.core.Utils.generateName("file"),
            preview: false,
            required: false,
            size: 1, //1 MB
            type: "file",
            proxy: [],
            valid: true,
            validator: null,
            value: "",
            columnName : null,
            originalType : null,
            data : null
        },
        initialize: function() {
            var data;
            this.set("label", this.checkHTMLtags(this.get("label")));
            this.set("defaultValue", this.checkHTMLtags(this.get("defaultValue")));
            this.on("change:label", this.onChangeLabel, this);
            this.initControl();
            this.set("items", []);
            this.set("proxy", []);

            this.set("validator", new PMDynaform.model.Validator({
                "type"  : "file",
                "required" : this.get("required")
            }));

            data = this.get("data");
            if ( data && (typeof data === "object") && (this.get("group") !== "grid") ) {
                if ( !jQuery.isArray(data["value"]) ) {
                    data["value"] = JSON.parse(data["value"]);
                }
                if ( !jQuery.isArray(data["label"]) && data["value"].length ) {
                    data["label"] = JSON.parse(data["label"]);
                }else{
                    data["label"] = [];
                }
                this.set( "data", data );
            } else {
                this.set("data",{
                    value : [],
                    label : []
                })
            }
            return this;
        },
        initControl: function() {
            if (this.get("dnd")) {
                //this.set("preview", true);
            }
            return this;
        },
        isValid: function() {
            this.get("validator").verifyValue();
            if (this.get("value").trim().length){
                return true;
            }else{
                return false;
            }
        },
        uploadSuccess: function (a, b, c) {
            //console.log("SUCCESS",a, b, c);

            return this;
        },
        uploadFailure: function (a, b, c) {
            //console.log("FAILURE",a, b, c);

            return this;
        },

        uploadFile: function (indexItem) {
            var file = this.get("items")[indexItem].file,
            rand = Math.floor((Math.random()*100000)+3),
            that = this,
            proxy = this.get("proxy"),
            proxyItem,
            formdata = new FormData();

            if (formdata) {
                formdata.append("images[]", file);

                proxyItem = $.ajax({
                    url: "server.php",
                    type: "POST",
                    data: formdata,
                    processData: false,
                    contentType: false,
                    onprogress: function (progress) {
                        // calculate upload progress
                        var percentage = Math.floor((progress.total / progress.totalSize) * 100);
                        // log upload progress to console
                        //console.log('progress', percentage);
                        if (percentage === 100) {
                          //console.log('DONE!');
                        }
                    },
                    success: that.uploadSuccess,
                    failure: that.uploadFailure
                });
                proxy.push(proxyItem);
                this.set("proxy", proxy);
            }
            
            return this;
        },
        stopUploadFile: function (index) {
            var proxy = this.get("proxy");

            proxy[index].abort();
            return this;
        }
	});

	PMDynaform.extendNamespace("PMDynaform.model.File", File);
}());