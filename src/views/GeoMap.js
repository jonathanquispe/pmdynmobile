(function(){
    
    var GeoMapView = PMDynaform.view.Field.extend({
        template: _.template($("#tpl-map").html()),
        validator: null,
        events: {
            "click .pmdynaform-map-fullscreen button": "applyFullScreen"
        },
        initialize: function (attributes){
            var that = this;
            //this.model.on("change", this.render, this);
            
        },
        onLoadGeoLocation: function () {
            if (this.model.get("currentLocation") && this.model.get("supportNavigator")) {
                this.geoLocation();
            } else {
                this.onLoadLocation();
            }
            return this;
        },
        geoLocation: function () {
            var that = this;

            navigator.geolocation.getCurrentPosition(function(position){
                that.model.set("latitude", position.coords.latitude);
                that.model.set("longitude", position.coords.longitude);
                that.onLoadLocation();
            });

            return this;
        },
        onLoadLocation: function () {
            var that = this,
            coords, 
            mapOptions,
            map,
            marker,
            canvasHTML = that.$el.find(".pmdynaform-map-canvas")[0];

            coords = new google.maps.LatLng(this.model.get("latitude"), this.model.get("longitude"));
            mapOptions = {
                zoom: this.model.get("zoom"),
                center: coords,
                panControl: this.model.get("panControl"),
                zoomControl: this.model.get("zoomControl"),
                scaleControl: this.model.get("scaleControl"),
                streetViewControl: this.model.get("streetViewControl"),
                overviewMapControl: this.model.get("overviewMapControl"),
                mapTypeControl: this.model.get("mapTypeControl"),
                navigationControlOptions: {
                    style: google.maps.NavigationControlStyle.SMALL
                },
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            map = new google.maps.Map(canvasHTML, mapOptions);
            this.model.set("googlemap", map);
            
            marker = new google.maps.Marker({
              position: coords,
              map: map,
              draggable: this.model.get("dragMarker"),
              title: ""
            });
            google.maps.event.addListener(marker, 'dragend', function(event) {
                that.model.set("latitude", event.latLng.lat().toFixed(that.model.get("decimals")));
                that.model.set("longitude", event.latLng.lng().toFixed(that.model.get("decimals")));
                
            });
            this.model.set("marker", marker);
            //this.rightToLeftLabels();
            
            return this;
        },
        applyFullScreen: function () {
        	
            if (this.fullscreen.supported) {
            	this.fullscreen.toggle();
            } else {
            	this.$el(".pmdynaform-map-fullscreen").hide();
            }
            
            return this;
        },
        render: function () {
        	var that = this,
        	canvasMap;

        	that.$el.html( that.template( that.model.toJSON()) );
        	canvasMap = that.$el.find(".pmdynaform-map-canvas");
            this.onLoadGeoLocation();        		
            if (this.model.get("fullscreen")) {
            	this.fullscreen = new PMDynaform.core.FullScreen({
                element: this.$el.find(".pmdynaform-map-canvas")[0],
                onReadyScreen: function() {
		            setTimeout(function() {
		                that.$el.find(".pmdynaform-map-canvas").css("height", $(window).height() + "px");
		            }, 500);
		        },
		        onCancelScreen: function() {
		            setTimeout(function() {
		                that.$el.find(".pmdynaform-map-canvas").css("height", "");
		            }, 500);
		        }
            });
            }

			return this;
        }
    });

    PMDynaform.extendNamespace("PMDynaform.view.GeoMap", GeoMapView);
}());
