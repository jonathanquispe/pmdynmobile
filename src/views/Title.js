(function(){
    
    var Title = PMDynaform.view.Field.extend({
        template: null,
        validator: null,
        etiquete: {
            title: _.template($("#tpl-label-title").html()),
            subtitle: _.template($("#tpl-label-subtitle").html())
        },
        initialize: function (){
            var type = this.model.get("type");
            this.template = this.etiquete[type];
            
            this.model.on("change", this.render, this);
        },
        render: function() {
            this.$el.html( this.template(this.model.toJSON()) );
            return this;
        }
    });
    
    PMDynaform.extendNamespace("PMDynaform.view.Title", Title);
}());
