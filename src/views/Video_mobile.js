(function(){
    var Video_mobile = PMDynaform.view.Field.extend({
        validator: null,
        template: _.template($("#tpl-Video_mobile").html()),
        initialize: function (){
            this.model.on("change", this.render, this);
        },
        render: function() {
            this.$el.html( this.template(this.model.toJSON()) );
            return this;
        }
    });
    PMDynaform.extendNamespace("PMDynaform.view.Video_mobile", Video_mobile);
}());
