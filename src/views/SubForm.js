(function(){
	var SubFormView = Backbone.View.extend({
        template:_.template($('#tpl-form').html()),
        formView: null,
        availableElements: null,
        parent: null,
		initialize: function (options) {
			var availableElements = [
                "text",
                "textarea",
                "checkbox",
                "radio",
                "dropdown",
                "button",
                "datetime",
                "fieldset",
                "suggest",
                "link",
                "hidden",
                "title",
                "subtitle",
                "label",
                "empty",
                "file",
                "image",
                "grid"
            ];
            this.availableElements = availableElements;
            if(options.project) {
                this.project = options.project;
            }
            this.checkItems();
			this.makeSubForm();

		},
        checkItems: function () {
            var i,
            j,
            newItems = [],
            row = [],
            json = this.model.toJSON();

            if (json.items) {
                for (i=0; i<json.items.length; i+=1) {
                    row = [];
                    for(j=0; j<json.items[i].length; j+=1){
                        if ($.inArray(json.items[i][j].type, this.availableElements) >=0) {
                            row.push(json.items[i][j]);
                        }
                    }
                    if (row.length > 0) {
                        newItems.push(row);
                    }
                }
            }

            json.items = newItems;
            this.model.set("modelForm", json);
            
            return this;
        },
		makeSubForm: function() {
            var panelmodel = new PMDynaform.model.FormPanel(this.model.get("modelForm"));

            this.formView = new PMDynaform.view.FormPanel({
                model: panelmodel, 
                project: this.project
            });

            return this;
        },
        validate: function () {
            this.isValid();
        },
        getItems: function () {
            return this.formView.items.asArray();;
        },
        isValid: function () {
            var i, formValid = true,
            itemsField = this.formView.items.asArray();

            if (itemsField.length > 0) {
                for (i = 0; i < itemsField.length; i+=1) {
                    if(itemsField[i].validate) {
                        itemsField[i].validate(event);
                        if (!itemsField[i].model.get("valid")) {
                            formValid = itemsField[i].model.get("valid");
                        }
                    }
                }
            }
            this.model.set("valid", formValid );

            return formValid;
        },
        setData: function (data) {
            //using the same method of PMDynaform.view.FormPanel
            this.formView.setData(data);

            return this;
        },
        getData: function () {
            var i,
            k,
            field,
            fields,
            panels,
            formData;
            
            formData = this.model.getData();

                fields = this.formView.items.asArray();
                for (k=0; k<fields.length; k+=1) {
                    if ((typeof fields[k].getData === "function") &&
                        (fields[k] instanceof PMDynaform.view.Field)) {
                        //formData.fields.push(fields[k].getData());
                        field = fields[k].getData();
                        formData.variables[field.name] = field.value;
                    }
                }
            
            return formData;
        },
        render: function () {
            this.$el.html( this.template(this.model.toJSON()) );
            this.$el.find(".pmdynaform-field-form").append(this.formView.render().el);

            return this;
        }
	});
	
	//.pmdynaform-formcontainer
	PMDynaform.extendNamespace("PMDynaform.view.SubForm", SubFormView);
	
}());
