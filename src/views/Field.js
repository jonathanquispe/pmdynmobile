(function(){
	var FieldView = Backbone.View.extend({
		tagName: "div",
        events : {
                "click .form-control": "onclickField",
            },
		initialize: function (options) {
			if(options.project) {
                this.project= options.project;
            }

			this.setClassName()
				.render();
		},
		setClassName: function() {
			//this.$el.addClass(this.model.get("container").style.cssClasses.toString().replace(/,/g," "));			
			return this;
		},
		getData: function() {
			if( this.updateValueControl) {
				this.updateValueControl();
			}

            return this.model.getData();
        },
        enableTooltip: function(){
        	this.$el.find("[data-toggle=tooltip]").tooltip().click(function(e) {
                $(this).tooltip('toggle');
            });
        	return this;
        },
        applyStyleError: function () {
            this.$el.addClass("has-error has-feedback");
            return this;
        },
        applyStyleSuccess: function () {
            this.$el.removeClass("has-error");
            if (!this.model.get("disabled")) {
            	this.$el.addClass("has-success");
            }
            
            return this;
        },
        changeValuesFieldsRelated: function () {
            this.model.changeValuesFieldsRelated();
            return this;
        },
        /**
         * The method is only supported if the field have options. 
         * Checks if the value to sets is inside of the options property.
         */
        setValueToDomain: function () {
            var htmlElement = this.getHTMLControl();
            
            if (htmlElement.length && !this.model.attributes.disabled) {
                if (this.validator) {
                    this.validator.$el.remove();
                    this.$el.removeClass('has-error');
                }
                
                if(!this.model.isValid()){    
                    this.validator = new PMDynaform.view.Validator({
                        model: this.model.get("validator")
                    });
                    
                    htmlElement.parent().append(this.validator.el);
                    this.applyStyleError();
                }
            }

            return this;
        },
        /**
         * Apply Javascript events associated to control
         *
         */
        on: function (e, fn) {
        	var that = this, 
        	control = this.$el.find("input");

        	if (control) {
        		control.on(e, function(event){
	        		fn(event, that);

	        		event.stopPropagation();
	        	});
        	} else {
        		throw new Error ("Is not possible find the HTMLElement associated to field");
        	}
        	
        	return this;
        },
        /**
         * The method is just for return the Jquery HTML of the control 
         * @return {JQuery HTMLElement} Encapsulate the HTMLElement
         */
        getHTMLControl: function () {
            return this;
        },
		render: function() {
			this.$el.html( this.template(this.model.toJSON()) );
			if (this.model.get("hint")) {
				this.enableTooltip();
			}
            this.setValueToDomain();
			return this;
		},
        onclickField : function (){
            console.log("click en field");
            return this;
        },
        setLabel : function (label) {
            var tagLabel;
            if (this.model.attributes.label !== undefined) {
                this.model.attributes.label = label;
                if (this.el || this.$el.length) {
                    this.$el.find("label").find("span[class='textlabel']").text(label);
                    this.$el.find("h4").find("span[class='textlabel']").text(label);
                    this.$el.find("h5").find("span[class='textlabel']").text(label);
                }
            } else {
                throw new Error("is not supported label property in " + this.model.get("type")+" field" );
            }
            return this;
        },
        getLabel : function () {
            if (this.model.get("label") !== undefined){
                return this.model.get("label");
            }
            throw new Error("is not supported label property in " + this.model.get("type")+" field" );
        },
        setValue : function (value) {
            if ( this.model.attributes.value !== undefined ) {
                this.model.set("clickedControl",false);
                this.model.set("value",value);
                this.render(true);
                if (this.model.get("validator")){
                    this.validate();
                }
            }
            return this;
        },
        getInfo : function () {
            return this.model.toJSON();
        },
        getValue : function () {
            if (this.model.get("value") !== undefined){
                return this.model.get("value");
            }
            throw new Error("is not supported label property in " + this.model.get("type")+" field" );
        },
        setHref : function (value) {
            this.model.set("href",value)
            return this;
        },        
        getDataType : function () {
            return this.model.get("dataType") || null;
        },
        getControlType : function () {

        },
        setNameHiddenControl : function (){
            var hidden;
            if(this.el){
                if (this.model.get("group") === "grid") {
                    hidden = this.$el.find("input[type = 'hidden']")[0];
                    name = this.model.get("name");
                    name = name.substring(0,name.length-1).concat("_label]");
                    hidden.name = hidden.id = "form" + name;
                }else{
                    this.$el.find("input[type='suggest']")[0].name = "form[" + this.model.get("name")+"_label]";
                }
            }
            return this;
        },
        verifyData : function (){
            /*var data = {}, value;
            if ( this.model.get("value") && this.model.get("value").trim().length ) {
                data["value"] = this.model.get("value");
                data["label"] = this.model.get("label");
                this.model.set("data",data); 
            }
            return this;*/
        }
	});	

	PMDynaform.extendNamespace("PMDynaform.view.Field",FieldView);
}());
