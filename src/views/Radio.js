(function(){
	var RadioView = PMDynaform.view.Field.extend({
		clicked: false,
		previousValue : null,
		template: _.template( $("#tpl-radio").html()),
		events: {
	        "click input": "onChange",
	        "blur input": "validate",
	        "keydown input": "preventEvents"
	    },
		initialize: function (){
			this.model.on("change", this.checkBinding, this);
		},
		preventEvents: function (event) {
            //Validation for the Submit event
            if (event.which === 13) {
                event.preventDefault();
                event.stopPropagation();
            }
            return this;
        },
        onChangeCallback: function (){},
        setOnChange : function (fn) {
            if (typeof fn === "function") {
                this.onChangeCallback = fn;
            }
            return this;
        },
		checkBinding: function () {
			var form = this.model.get("form");
            
            if ( typeof this.onChangeCallback === 'function' ) {
                this.onChangeCallback(this.getValue(), this.previousValue);
            }

            if ( form && form.onChangeCallback) {
                form.onChangeCallback(this.model.get("name"), this.model.get("value"), this.previousValue);
            }

			if (!this.clicked) {
				this.render();
				//this.validate();
			}
			return this;
		},
		render : function () {
			this.$el.html(this.template(this.model.toJSON()));
			if (this.model.get("hint")) {
				this.enableTooltip();
			}
			this.setValueToDomain();
			this.previousValue = this.model.get("value");
			if (this.model.get("name").trim().length === 0){
				this.$el.find("input[type='radio']").attr("name","");
				this.$el.find("input[type='hidden']").attr("name","");
			}
			return this;
		},
		validate: function() {
			if (!this.model.get("disabled")) {
				this.previousValue = this.model.get("value");
				this.model.set({},{validate: true});
				if (this.validator) {
	                this.validator.$el.remove();
	                this.$el.removeClass('has-error has-feedback');
	            }
				if(!this.model.isValid()){
	                this.validator = new PMDynaform.view.Validator({
	                    model: this.model.get("validator")
	                });
	                this.$el.find(".pmdynaform-control-radio-list").parent().append(this.validator.el);
	                this.applyStyleError();
	            }
			}
			this.clicked = false;
			return this;
		},
		updateValueControl: function () {
            var i,
            inputs = this.$el.find("input");
            
            for (i=0; i<inputs.length; i+=1) {
            	if (inputs[i].checked) {
            		this.model.setItemClicked({
						value: inputs[i].value,
						checked: true
					});
            	}
            }
        	
            return this;
        },
		onChange: function (event) {
			var hidden, controls, label, i;
			this.clicked = true;
			this.model.setItemClicked({
				value: event.target.value,
				checked: event.target.checked
			});

			this.validate();
			this.changeValuesFieldsRelated();
			hidden = this.$el.find("input[type='hidden']");
			controls = this.$el.find("input[type='radio']");
			if (hidden.length && controls.length && this.model.get("value")){
				for ( i = 0 ; i < this.model.get("options").length ; i +=1 ) {
					if (this.model.get("options")[i]["value"] === this.model.get("value")){
						hidden.val(this.model.get("options")[i]["label"]);
						break;
					}
				}
			}
		},
		getHTMLControl: function () {
            return this.$el.find(".pmdynaform-control-radio-list");
        }
	});

	PMDynaform.extendNamespace("PMDynaform.view.Radio",RadioView);
}());
