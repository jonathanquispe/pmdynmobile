(function(){
	var TextareaView = PMDynaform.view.Field.extend({
		template: _.template( $("#tpl-textarea").html()),
        validator: null,
        keyPressed: false,
        previousValue : "",
        events: {
                "blur textarea": "validate",
                //"keyup textarea": "validate",
                "keydown textarea": "refreshBinding"
        },
        onChangeCallback: function (){},
        setOnChange : function (fn) {
            if (typeof fn === "function") {
                this.onChangeCallback = fn;
            }
            return this;
        },
        initialize: function (){
            var that = this;
            this.model.on("change", this.checkBinding, this);
        },
        refreshBinding: function () {
            this.keyPressed = true;
            return this;
        },
        checkBinding: function () {
            var form = this.model.get("form");
            if (this.model.get("operation")){
                this.onChangeCallbackOperation(this);
            }
            if ( typeof this.onChangeCallback === 'function' ) {
                this.onChangeCallback(this.getValue(), this.previousValue);
            }

            if ( form && form.onChangeCallback ) {
                form.onChangeCallback(this.model.get("name"), this.model.get("value"), this.previousValue);
            }

            //If the key is not pressed, executes the render method
            if (!this.keyPressed) {
                this.render();
            }
        },
        render : function () {
        	var hidden, name;
            this.$el.html( this.template(this.model.toJSON()) );
            if (this.model.get("hint") !== "") {
                this.enableTooltip();
            }
            if (this.model.get("group") === "grid") {
                hidden = this.$el.find("input[type = 'hidden']")[0];
                name = this.model.get("name");
                name = name.substring(0,name.length-1).concat("_label]");
                hidden.name = hidden.id = "form" + name;
            }
            this.previousValue = this.model.get("value");

            if (this.model.get("name").trim().length === 0){
                this.$el.find("input[type='textarea']").attr("name","");
                this.$el.find("input[type='hidden']").attr("name","");
            }
            return this;
        },
        validate: function(event){
            if (event) {
                if ((event.which === 9) && (event.which !==0)) { //tab key
                    this.keyPressed = true;
                }
            }
            if (!this.model.get("disabled")) {
                this.model.set({value: this.$el.find("textarea").val()}, {validate: true});
                this.$el.find("input").val(this.model.get("value"));
                if (this.validator) {
                    this.validator.$el.remove();
                    this.$el.removeClass('has-error');
                }
                if(!this.model.isValid()){
                    this.validator = new PMDynaform.view.Validator({
                        model: this.model.get("validator")
                    });  
                    this.$el.find("textarea").parent().append(this.validator.el);
                    this.applyStyleError();
                }
            }
            this.changeValuesFieldsRelated();
            this.keyPressed = false;
            this.previousValue = this.model.get("value");
            return this;
        },
        updateValueControl: function () {
            var inputVal = this.$el.find("textarea").val();

            this.model.set("value", inputVal);

            return this;    
        },
        on: function (e, fn) {
            var that = this, 
            control = this.$el.find("textarea");

            if (control) {
                control.on(e, function(event){
                    fn(event, that);

                    event.stopPropagation();
                });
            }
            
            return this;
        },
        getHTMLControl: function () {
            return this.$el.find("textarea");
        }
	});

	PMDynaform.extendNamespace("PMDynaform.view.TextArea",TextareaView);
		
}());

