(function(){
	var HiddenModel = PMDynaform.view.Field.extend({
		template: _.template( $("#tpl-hidden").html()),
		render: function(isConsole) {
			var data = {}, hidden;
			if ( isConsole ) {
				data["value"] = this.model.get("value");
				data["label"] = this.model.get("value");
				this.model.attributes.data = data;
			}
			this.$el.html( this.template(this.model.toJSON()) );
			if (this.model.get("group") === "grid") {
				hidden = this.$el.find("input[type = 'hidden']")[1];
				name = this.model.get("name");
				name = name.substring(0,name.length-1).concat("_label]");
				hidden.name = hidden.id = "form" + name;
				hidden.value = this.model.get("value"); 
			}
			if (this.model.get("name").trim().length === 0){
				this.$el.find("input[type='hidden']").attr("name","");
			}
			return this;
		}
	});

	PMDynaform.extendNamespace("PMDynaform.view.Hidden", HiddenModel);
	
}());
