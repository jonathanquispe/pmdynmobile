(function(){
	var Empty = Backbone.View.extend({
		item: null,
		template: _.template( $("#tpl-empty").html()),
		render: function() {
			this.$el.html( this.template(this.model.toJSON()) );
			return this;
		}
	});

	PMDynaform.extendNamespace("PMDynaform.view.Empty",Empty);
	
}());
