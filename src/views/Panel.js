(function(){
    var PanelView = Backbone.View.extend({
        content : null,
        colsIndex: null,
        template: null,
        collection: null,
        items: null, 
        views: [],
        renderTo: document.body,
        project: null,
        initialize: function(options) {
            var i, defaults = {
                factory: {
                    products: {
                        "form": {
                            model: PMDynaform.model.FormPanel,
                            view: PMDynaform.view.FormPanel
                        },
                        "fieldset": {
                            model: PMDynaform.model.Fieldset,
                            view: PMDynaform.view.Fieldset
                        },
                        "panel": {
                            model: PMDynaform.model.FormPanel,
                            view: PMDynaform.view.FormPanel
                        }
                    },
                    defaultProduct: "form"
                }
            };
            if (options.renderTo) {
                this.renderTo = options.renderTo;
            }
            if(options.project) {
                this.project = options.project;
            }
            this.views = [];

            this.makePanels();
            this.render();
            for ( i = 0 ; i < this.views.length ; i+=1 ) {
                this.views[i].runningFormulator();
            }
        },
        getData: function () {
            var i, 
            k, 
            field,
            subform,
            fields, 
            panels,
            formData;

            panels = this.model.get("items");
            formData = this.model.getData();
            for (i = 0; i < panels.length; i+=1) {
                fields = this.views[i].items.asArray();
                for (k = 0; k < fields.length; k+=1) {

                    if ( (typeof fields[k].model.getData === "function") && (fields[k].model.attributes.type === "form") ){
                        subform = fields[k].getData();
                        $.extend(true, formData.variables, subform.variables);
                    } else if (typeof fields[k].model.getData === "function"){
                        field = fields[k].model.getData();
                        formData.variables[field.name] = field.value;
                    }

                    /*if ((typeof fields[k].getData === "function") && 
                        (fields[k] instanceof PMDynaform.view.Field)) {
                        field = fields[k].getData();
                        formData.variables[field.name] = field.value;
                    } else if ((typeof fields[k].getData === "function") && 
                        (fields[k] instanceof PMDynaform.view.SubForm)) {
                        subform = fields[k].getData();
                        $.extend(true, formData.variables, subform.variables);
                    }*/
                }
            }

            return formData;
        },
        getData2: function () {
            var i, 
            k, 
            field,
            subform,
            fields, 
            panels,
            formData,
            grid,
            data = {};

            panels = this.model.get("items");

            for (i = 0; i < panels.length; i+=1) {
                fields = this.views[i].items.asArray();
                for (k = 0; k < fields.length; k+=1) {
                    if ( (typeof fields[k].model.getData === "function") && (fields[k].model.attributes.type === "form") ){
                    } else if (typeof fields[k].model.getData === "function"){
                        if (fields[k].model.get("type") === "grid") {
                            grid = fields[k].model;
                            data[grid.get("name")] = fields[k].getData2(); 

                        } else {
                            field = fields[k].model.getData();
                            data[field.name] = field.value;
                        }
                    }
                }
            }   
            return data;
        },
        setData2 : function (data) {
            this.getPanels()[0].setData2(data);
            return this;
        },
        makePanels: function() {  
            var i = 0,
            items,
            panelmodel,
            view;

            this.views = [];
            items = this.model.get("items");

            for(i=0; i<items.length; i+=1){
                if ($.inArray(items[i].type, ["panel","form"]) >= 0) {

                    panelmodel = new PMDynaform.model.FormPanel(items[i]);
                    
                    view = new PMDynaform.view.FormPanel({
                        model: panelmodel,
                        project: this.project
                    });
                    this.views.push(view);
                }
            }

            return this;
        },
        getPanels: function () {
            var items = (this.views.length > 0) ? this.views : [];
            
            return items;
        },
        render: function () {
            var i,
            j;

            this.$el = $(this.el);
            for(i=0; i<this.views.length; i+=1){
                this.$el.append(this.views[i].render().el);
            }
            this.$el.addClass("pmdynaform-container");
            $(this.renderTo).append(this.el);

            return this;
        },
        afterRender: function () {
            var i;

            for(i=0; i<this.views.length; i+=1) {
                this.views[i].afterRender();
            }

            return this;
        }
    });
    PMDynaform.extendNamespace("PMDynaform.view.Panel", PanelView);
    
}());
