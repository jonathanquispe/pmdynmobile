(function(){
	var GridView = PMDynaform.view.Field.extend({
		block: true,
		template: _.template( $("#tpl-grid").html()),
		templatePager: _.template( $("#tpl-grid-pagination").html() ),
		templateTotal: _.template( $("#tpl-grid-totalcolumn").html() ),
	    colSpanLabel: 3,
        colSpanControl: 9,
        gridtable: [],
        flagRow: 0,
        dom: [],
        row: [],
        cols: [],
        showPage: 1,
        items: [],
        numberRest: 0,
        rest: 0,
        priority: {
			file: 1,
			image: 2,
			radio: 3,
			checkbox: 4,
			textarea: 5,
			datetime:6,
			dropdown: 7,
			text: 8,
			button: 9,
			link: 10,
			defect: 0
		},
        titleHeader: [],
		indexResponsive : "3%",
		removeResponsive : "3%",
        thereArePriority: 0,
        onRemoveRowCallback: function(){},
        onAddRowCallback: function(){},
        onClickPageCallback: function(){},
        events: {
                "click .pmdynaform-grid-newitem": "onClickNew",
                "click .pagination li": "onClickPage"
        },
        requireVariableByField: [
            "text",
            "textarea",
            "checkbox",
            "radio",    
            "dropdown",
            "datetime",
            "suggest",
            "link",
            "hidden",
            "label"
        ],
        factory : {},
		initialize: function (options) {
			var factory = {
	            products: {
	                "text": {
	                    model: PMDynaform.model.Text,
	                    view: PMDynaform.view.Text
	                },
	                "textarea": {
	                    model: PMDynaform.model.TextArea,
	                    view: PMDynaform.view.TextArea
	                },
	                "checkbox": {
	                    model: PMDynaform.model.Checkbox,
	                    view: PMDynaform.view.Checkbox
	                },
	                "radio": {
	                    model: PMDynaform.model.Radio,
	                    view: PMDynaform.view.Radio
	                },
	                "dropdown": {
	                    model: PMDynaform.model.Dropdown,
	                    view: PMDynaform.view.Dropdown
	                },
	                "button": {
	                    model: PMDynaform.model.Button,
	                    view: PMDynaform.view.Button
	                }, 
	                "datetime": {
	                    model: PMDynaform.model.Datetime,
	                    view: PMDynaform.view.Datetime
	                },
	                "suggest": {
	                    model: PMDynaform.model.Suggest,
	                    view: PMDynaform.view.Suggest
	                },                                        
	                "link": {
	                    model: PMDynaform.model.Link,
	                    view: PMDynaform.view.Link
	                },                                        
	                "file": {
	                    model: PMDynaform.model.File,
	                    view: PMDynaform.view.File
	                },
	                "label": {
                        model: PMDynaform.model.Label,
                        view: PMDynaform.view.Label
                    },
	                "hidden": {
                    	model: PMDynaform.model.Hidden,
                        view: PMDynaform.view.Hidden
                    }
	            },
	            defaultProduct: "text"
	        },
	        k,
	        rows = parseInt(this.model.get("rows"), 10);

	        this.items = [];
	        this.row = [];
	        this.dom = [];
	        this.cols = [];
	        this.showPage = 1;
			this.gridtable = [];
			this.titleHeader = [];
			this.checkColSpanResponsive();
			this.setFactory(factory);
			this.buildColumns({
				executeInit: true
			});
			for (k=0; k<rows; k+=1) {
				this.addRow();
			}
			this.model.attributes.titleHeader = this.titleHeader;
			
		},
		buildColumns: function () {
			var row;
			row = this.makeColumns({
            	executeInit: true
            });
            this.model.attributes.dataColumns = row.model;
            this.model.attributes.totalRow = row.data;
            this.items = [];
            this.model.attributes.gridFunctions = [];
			return this;
		},
		onClickNew: function () {
			var currentRows = this.model.get("rows"),
			newItem;

			this.block = true;
			//this.model.set("rows", parseInt(currentRows + 1, 10));
			this.model.attributes.rows = parseInt(currentRows + 1, 10);
			newItem = this.addRow();
			this.renderGridTable(true);
			//Calling to callBack associated
			this.onAddRowCallback(newItem, this);

			return this;
		},
		makeTitleHeader: function (columns) {
			var j,
			nroLabel = "Nro";
			
			this.titleHeader.push(nroLabel);
			for (j=0; j<columns.length; j+=1) {
				this.titleHeader.push(columns[j].title);
			}

			return this;
		},
		setTitleHeader: function (titles) {
			var j;
			
			for (j = 0; j < titles.length; j+=1) {
				this.titleHeader.push(titles[j]);
			}

			return this;
		},
		verifyPageNumber: function () {
			var i,
			rows = this.model.get("rows"),
			size = this.model.get("pageSize"),
			pagerItems,
			currentPage = this.showPage,
			children = this.$el.find(".pmdynaform-grid-tbody").children();

			if (children.length > 0) {
				for (i=0; i<children.length; i+=1) {
					if (children[i].className.indexOf("active") > 0) {
			 			currentPage = i+1;
						break;
					}
				}
			}

			pagerItems = Math.ceil(rows/size) ? Math.ceil(rows/size) : 1;
			if (currentPage > pagerItems) {
				currentPage-=1;
			}
			this.showPage = currentPage;

			return this;
		},
		addRow: function () {
			var i,
			size = this.gridtable.length,
            row,
            product,
            rowData;
            rowData = this.model.get("data");
            if ( rowData && (this.gridtable.length < this.model.get("rows"))) {
				rowData = rowData[this.gridtable.length+1]
	            row = this.makeColumns({
	            	executeInit: false
	            }, rowData);            	
            } else {
	            row = this.makeColumns({
	            	executeInit: false
	            });
            }

            this.model.attributes.gridFunctions.push(row.data);
            this.gridtable.push(row.view);

			return row;
		},
		removeRow: function (row) {
			var currentRows = this.model.get("rows"),
			itemRemoved;

			itemRemoved = this.gridtable.splice(row, 1);
			this.dom.splice(row, 1);
			this.model.attributes.rows = parseInt(currentRows - 1, 10);
			
			return itemRemoved;
		},
		validateVariableField: function (field) {
            var isOk = false;

            if ($.inArray(field.type, this.requireVariableByField) >= 0) {
                if (field.var_uid) {
                    isOk = true;
                }
            } else {
                isOk = "NOT";
            }

            return isOk;
        },
		makeColumns: function (properties, rowData) {
			var that = this,
			columns = this.model.get("columns"),
			//dataColumns = this.model.get("dataColumns"),
			data = this.model.get("data"),
			columnModel,
			suc,
			rowData = rowData || [],
			colSpanControl,
            factory = this.factory,
            size = this.gridtable.length,
            product,
            newNameField,
            newIdField,
            rowView = [],
            rowModel = [],
            productModel,
            variableEnabled,
            productBuilt,
            jsonFixed,
            mergeModel,
            newDependentFields,
            size = this.gridtable.length,
            i,
            parentItems = new PMDynaform.util.ArrayList();
	            for (i = 0; i < columns.length; i+=1) {
	            	newNameField = "";
	            	mergeModel = columns[i] ;
	            	mergeModel.data = rowData[i];
	            	mergeModel.name = mergeModel.name;
	            	mergeModel.formula = mergeModel.formula;
	            	mergeModel.dependentFields = mergeModel.dependentFields;
	            	mergeModel.form = this.model.get("form") || null;
					
					if ( mergeModel.mode && mergeModel.mode === "parent" ) {
						mergeModel.mode = this.model.get("mode");
					}

	            	if ( (mergeModel.originalType === "checkbox"  || mergeModel.type === "checkbox" ) && mergeModel.mode === "view" ) {
	            		mergeModel.mode = "disabled";
	            	}

	            	mergeModel.executeInit = properties.executeInit;

	            	jsonFixed  = new PMDynaform.core.TransformJSON({
	                    parentMode: this.model.get("parentMode"),
	                    field: mergeModel
	                });
		            if (jsonFixed.getJSON().type) {
		                product =   factory.products[jsonFixed.getJSON().type.toLowerCase()] ? 
		                    factory.products[jsonFixed.getJSON().type.toLowerCase()] : factory.products[factory.defaultProduct];
		            } else {
		                product = factory.products[factory.defaultProduct];
		            }
		            /**
	            	 * The executeInit property is for enable or disable the execution 
	            	 * of the query associated to variable (field)
	            	 **/
					/*if (this.model.get("layout") === "responsive") {
	            		colSpanControl = this.colSpanControlFieldResponsive(columns);
	            	} else {*/
						colSpanControl = this.colSpanControlField(columns, jsonFixed.getJSON().type, i);
					//}

	            	/**
	            	 * The current method is for check if the controls needs the variable parameter
	            	 * for execute its model
	            	 */
	            	variableEnabled = this.validateVariableField(mergeModel);
		            columnModel = {
	            		colSpanLabel: 4,
	                	colSpanControl: (this.model.get("layout") === "form") ? 8:colSpanControl,
	                	colSpan: colSpanControl,
	                	label: mergeModel.title,
	                	title: mergeModel.title,
	                	layout: this.model.get("layout"),
	                	width: "200px",
		                project: this.model.get("project"),
		                namespace: this.model.get("namespace"),
		                mode: this.model.get("mode"),
		                variable: (variableEnabled !== "NOT")? this.getVariable(mergeModel.var_uid) : null,
		                _extended: {
		                	name: mergeModel.name || PMDynaform.core.Utils.generateName("radio"),
		                	id: mergeModel.id || PMDynaform.core.Utils.generateID(),
		                	dependentFields: mergeModel.dependentFields,
		                	formula: mergeModel.formula || null
		                },
		                group: "grid",
		                columnName : mergeModel.name || PMDynaform.core.Utils.generateName("radio"),
		                originalType : mergeModel.type
		            };
	            	jQuery.extend(true, columnModel, jsonFixed.getJSON());
	            	columnModel.row = this.gridtable.length;
	            	columnModel.col = i;
					if (mergeModel.type == "dropdown" || mergeModel.type == "suggest") {
						columnModel.options = mergeModel.options; 
					}
					productModel = new product.model(columnModel);
					//Step for change the name to field
					//newNameField = this.changeNameField(productModel.get("_extended").name, this.gridtable.length+1, i+1);
					//newIdField = this.changeNameField(productModel.get("_extended").id, this.gridtable.length+1, i+1);
					newNameField = this.changeNameField(this.model.get("name"), this.gridtable.length+1, productModel.get("_extended").name);
					newIdField = this.changeIdField(this.model.get("name"), this.gridtable.length+1, productModel.get("_extended").name);
					productModel.attributes.name = newNameField;
					productModel.attributes.id = newIdField;
					
					rowModel.push(productModel);
					
		            productBuilt = new product.view({
		                model: productModel,
		            	project: this.project,
		            	parent: this
		            });
		            //Adding CallBack to TextField when the field is part of a column that has a function enabled
					if (productBuilt.model.get("operation")) {
						productBuilt.on("changeValues", function(){
							that.setValuesGridFunctions({
								row: this.model.attributes.row,
								col: this.model.attributes.col,
								data: this.model.attributes.value
							});
							that.createHTMLTotal();
						});
					}

		            parentItems.insert(productBuilt);
	            	rowView.push(productBuilt);

	            	if(!properties.executeInit) {
	            		this.items.push(productBuilt);
	            		rowData.push(0);
	            	}
	            }


            for (suc=0; suc<rowView.length; suc+=1) {
            	rowView[suc].parent = {
            		items: parentItems,
            		parent: this
            	};
            	
            }
            
        	this.updateNameFields(rowView);

			return {
				model: rowModel,
				view: rowView,
				data: rowData
			};
		},
		setValuesGridFunctions: function (field) {

			if (this.model.attributes.functions) {
				this.model.attributes.gridFunctions[field.row][field.col] = isNaN(parseFloat(field.data))? 0: parseFloat(field.data);
				this.model.applyFunction();
			}
			
			return this;
		},
		getVariable: function (var_uid) {
            var i,
            varSelected,
            variables = this.model.attributes.variables;

            loop_variables:
            for (i=0; i<variables.length; i+=1) {
                if (variables[i] && variables[i].var_uid === var_uid) {
                    varSelected = variables[i];
                    break loop_variables;
                }
            }

            return varSelected;
        },
		checkColSpanResponsive: function () {
			var i,
			columns = this.model.get("columns"),
			thereArePriority = 0,
			layout = this.model.get("layout");

			if (layout === "responsive" || layout === "form") {
				this.numberRest = 10%columns.length;

				if (this.numberRest > 0) {
					for (i=0; i<columns.length; i+=1) {
						if (this.priority[columns[i].type] <= 6) {
							thereArePriority +=1;
						}
					}
				}
				this.thereArePriority = thereArePriority;
			}
			

			return this;
		},
		colSpanControlField: function (columns, type, indexColumn) {
			var rest,
			itemsLength = columns.length,
			layout = this.model.get("layout"),
			defaultColSpan = 8;
			
			if (this.numberRest > 0) {
				if (this.priority[type] <= 6 && this.thereArePriority > 0) {
					defaultColSpan = parseInt(10/itemsLength) +1;
					this.numberRest -=1;
					this.thereArePriority -=1;
				} else {
					if (this.numberRest >= parseInt(itemsLength - indexColumn)) {
						defaultColSpan = parseInt(10/itemsLength) +1;
						this.numberRest -=1;
					} else {
						defaultColSpan = parseInt(10/itemsLength);
					}
				}
			} else {
				defaultColSpan = parseInt(10/itemsLength);
			}

			return defaultColSpan;
		},
		colSpanControlFieldResponsive : function  (columns) {
			var columnWidth = 100, res;
			res = parseInt(this.indexResponsive) + parseInt(this.removeResponsive);
			columnWidth = parseInt((columnWidth - res)/(columns.length-this.model.get("countHiddenControl")));
			return columnWidth;
		},

		/*changeNameField: function (nameform, row, column) {
			return nameform+"]["+row+"]["+column;

		},*/
		/*
		form[grid1][1][nombre]
		form[grid1][2][nombre]
		*/
		changeIdField : function (nameform, row, column){
			return "["+nameform+"]["+row+"]["+column+"]";
		},
		changeNameField : function (nameform, row, column){
			return "["+nameform+"]["+row+"]["+column+"]";
		},
		/*changeNameField: function (name, row, column) {	 
			return name + "_" + row + "_" + column;	
		},*/	
		updateNameFields: function (rowView) {
			var i,
			j, 
			k,
			l,
			label,
			formulaFields = "",
			dependentFields,
			newDependentFields = [];
			
			for (i=0; i< rowView.length; i+=1) {
				newDependentFields = [];
				dependentFields = rowView[i].model.get("dependentFields");
				if (dependentFields) {
					for (j=0; j < dependentFields.length; j+=1) {
						label = dependentFields[j];
						for (k=0; k< rowView.length; k+=1) {
							if ((label === rowView[k].model.get("_extended").name)
								&& ($.inArray(label, newDependentFields) < 0) ) {
								newDependentFields.push(rowView[k].model.get("name"));
							}
						}
					}	
					rowView[i].model.attributes.dependentFields = newDependentFields;
				}

				formulaFields = rowView[i].model.get("_extended").formula;
				if (typeof formulaFields === "string") {
					for (l=0; l< rowView.length; l+=1) {
						if (i !== l) {
							formulaFields = formulaFields.replace(new RegExp(rowView[l].model.get("_extended").name, 'g'), rowView[l].model.get("name"));
							rowView[i].model.attributes.formula = formulaFields;
							rowView[i].model.attributes.formulator.data = formulaFields;
						}
					}
				}
			}
				
			return newDependentFields;
		},
		setFactory: function (factory) {
            this.factory = factory;
            return this;
        },
		validate: function(event) {
			var i, 
			k, 
			gridpanel,
			fields,
			row = [],
			validGrid = true,
			gridpanel = this.gridtable;

			for (i=0; i<gridpanel.length; i+=1) {
				row = [];
				for (k=0; k<gridpanel[i].length; k+=1) {
					if(gridpanel[i][k].validate) {
                        gridpanel[i][k].validate(event);
                        if (!gridpanel[i][k].model.get("valid")) {
                            validGrid = gridpanel[i][k].model.get("valid");
                            this.model.set("valid",validGrid);
                            validGrid = false;
                        }
                    }
				}
			}
			this.model.set("valid",validGrid);
			return validGrid;
		},
		onRemoveRow: function (event) {
			var rowNumber, itemRemoved;

			if (event) {
				this.block = true;
				rowNumber = $(event.target).data("row");
				jQuery(this.dom[rowNumber]).remove();
				itemRemoved = this.removeRow(rowNumber);
				this.refreshButtonsGrid();
				
				this.renderGridTable();
				this.onRemoveRowCallback(itemRemoved, this);
			}

			return this;
		},
		onClickPage: function (event) {
			var objData = $(event.currentTarget.children).data(),
			parentNode = $(event.currentTarget).parent();

			parentNode.children().removeClass('active');
			$(event.currentTarget).addClass("active");

			this.onClickPageCallback(event, this);

			return this;
		},
		refreshButtonsGrid: function () {
			var i,
			tdNumber,
			buttonRemove,
			trs = this.dom;

			for (i=0; i<trs.length; i+=1) {
				// refresh html
				$(trs[i]).html();
				tdNumber = this.createRowNumber(i+1);
				buttonRemove = this.createRemoveButton(i);
				$(trs[i].firstChild).replaceWith( tdNumber );
				$(trs[i].lastChild).replaceWith( buttonRemove );
			}

			return this;
		},
		createRowNumber: function (index) {
			var tdNumber = document.createElement("div"),
			formgroup = document.createElement("div"),
			divNumber = document.createElement("div"),
			spanNumber = document.createElement("span"),
			label = document.createElement("label"),
			labelSpan = document.createElement("span"),
			containerField = document.createElement("div"),
			layout = this.model.get("layout"),
			tdRemove;

			tdNumber.className = (layout === "responsive") ? "col-xs-1 col-sm-1 col-md-1 col-lg-1": 
								"col-xs-12 col-sm-1 col-md-1 col-lg-1";

			if (layout === "responsive") {
				tdNumber.width = this.indexResponsive;
			}

			label.className = "hidden-lg hidden-md hidden-sm visible-xs control-label col-xs-4";
			labelSpan.innerHTML = "Nro";
			label.appendChild(labelSpan);

			divNumber.className = "col-xs-4 col-sm-12 col-md-12 col-lg-12 pmdynaform-grid-label rowIndex";
			spanNumber.innerHTML = index;
			divNumber.appendChild(spanNumber);
			if (layout === "form") {
				containerField.appendChild(label);	
				
				tdRemove = this.createRemoveButton(index-1);
				tdRemove.className = "col-xs-1 visible-xs hidden-sm hidden-md hidden-lg remove-row-form";
				tdRemove.style.cssText = "float: right; margin-right: 15%";
				containerField.appendChild(tdRemove);
			}

			containerField.appendChild(divNumber);
			formgroup.className = "row form-group";
			formgroup.appendChild(containerField);
			tdNumber.appendChild(formgroup);
			return tdNumber;
		},

		createRemoveButton: function (index) {
			var that = this,
			tdRemove,
			buttonRemove,
			layout = this.model.get("layout");

			tdRemove = document.createElement("div");
			tdRemove.className = (layout === "form") ? "pmdynaform-grid-removerow hidden-xs col-xs-1 col-sm-1 col-md-1 col-lg-1":
			(layout === "static") ? "pmdynaform-grid-removerow-static": "col-xs-1 col-sm-1 col-md-1 col-lg-1";
			
			buttonRemove = document.createElement("button");
			
			buttonRemove.className = "glyphicon glyphicon-trash btn btn-danger btn-sm";
			buttonRemove.setAttribute("data-row", index);

			$(buttonRemove).data("row", index);
			$(buttonRemove).on("click", function(event) {
				that.onRemoveRow(event);
			});

			tdRemove.appendChild(buttonRemove);
			return tdRemove;
		},
		createHTMLTitle: function (	) {
			//xxxx
			var k,
			dom,
			title,
			td,
			colSpan,
			label,
			layout = this.model.get("layout"),
			hint;

			dom = this.$el.find(".pmdynaform-grid-thead");
			td = document.createElement("div");
			label = document.createElement("span");
			
			if (layout === "static") {
				dom.addClass("pmdynaform-grid-thead-static");
				td.className = "pmdynaform-grid-field-static wildcard";
				td.style.minWidth = "inherit";
			} else {
				//For the case: responsive and form
				td.className = "col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center wildcard";
			}
			if (layout !== "form"){
				td.style.width = this.indexResponsive;
			}
			//label.innerHTML = "Nro";
			td.appendChild(label);
			dom.append(td);
			for (k=0; k< this.gridtable[0].length; k+=1) {
				if (this.gridtable[0][k].model.get("type")!=="hidden"){
					colSpan = this.gridtable[0][k].model.get("colSpan");
					title = this.gridtable[0][k].model.get("title");
					td = document.createElement("div");
					label = document.createElement("span");
					this.checkColSpanResponsive();
					colSpan = this.colSpanControlField(this.gridtable[0], this.gridtable[0][k].model.get("type"), k);
					td.className = (layout === "form")? "hidden-xs col-xs-"+colSpan+" col-sm-"+colSpan+" col-md-"+colSpan+" col-lg-" + colSpan + " text-center" : 
					(layout === "static")? "pmdynaform-grid-field-static": "col-xs-"+colSpan+" col-sm-"+colSpan+" col-md-"+colSpan+" col-lg-" + colSpan + " text-center";
					label.innerHTML = title;
					label.style.fontWeight = "bold";
					label.style.maginLeft = "2px";
					$(label).css({
						"text-overflow": "ellipsis",
						"white-space": "nowrap",
						"overflow": "hidden",
						"display": "inline-block",
						"width": "80%",
						"text-align" : "center"
					});
					if (layout === "responsive") {
						$(label).css({
							width : "70%"
						});
						$(td).css({
							width : this.colSpanControlFieldResponsive(this.gridtable[0])+"%"
						});
					}
					if(this.gridtable[0][k].model.get("required")){
						td.appendChild($("<span class='pmdynaform-field-required'>*</span>")[0]);
					}

					hint = document.createElement("span");
					
					td.appendChild(label);

					if(this.gridtable[0][k].model.get("hint") && this.gridtable[0][k].model.get("hint").trim().length){
						hint = document.createElement("span");
						hint.className = "glyphicon glyphicon-info-sign";
						hint.setAttribute("data-toggle","tooltip");
						hint.setAttribute("data-container","body");
						hint.setAttribute("data-placement","bottom");
						hint.setAttribute("data-original-title",this.gridtable[0][k].model.get("hint"));
						hint.style.float = "right";
						if (this.model.get("columns").length < 6 && (layout== "responsive" || layout== "form")){
							td.appendChild(hint);
						}else{
							if( layout === "static" ) {
								td.appendChild(hint);
							}else{
								label.setAttribute("data-toggle","tooltip");
								label.setAttribute("data-container","body");
								label.setAttribute("data-placement","bottom");
								label.setAttribute("data-original-title",this.gridtable[0][k].model.get("hint"));
							}
						}
					}
					dom.append(td);
				}
			}
			return this;
		},
		createHTMLPager: function () {
			var i,
			that = this,
			pager = this.templatePager({
				id: this.model.get("id"),
				paginationItems: this.model.get("paginationItems")
			}),
			pagerContainer = this.$el.find(".pmdynaform-grid-pagination");

			pagerContainer.children().remove();
			pagerContainer.append(pager);

			return this;
		},
		createHTMLTotal: function () {
			var k,
			dom,
			title,
			td,
			operation,
			colSpan,
			label,
			result,
			icon,
			hint,
			totalrow = this.model.get("totalrow"),
			layout = this.model.get("layout"),
			iconTotal = {
				sum: "&#8721;",
				avg: "&#935;",
				other: "&#989;"
			};

			if (totalrow.length) {
				dom = this.$el.find(".pmdynaform-grid-functions");
				dom.children().remove();
				td = document.createElement("div");
				label = document.createElement("span");
				
				if (layout === "static") {
					dom.addClass("pmdynaform-grid-thead-static");
				} else {
					//For the case: responsive and form
					td.className = "col-xs-1 col-sm-1 col-md-1 col-lg-1 text-center";
				}
				//label.innerHTML = "Nro";
				td.appendChild(label);
				dom.append(td);
				if (layout !== "form"){
					td.style.width = this.indexResponsive;
				}
				for (k=0; k< this.gridtable[0].length; k+=1) {
					colSpan = this.gridtable[0][k].model.get("colSpan");
					title = totalrow[k]? totalrow[k] : "";
					td = document.createElement("div");
					label = document.createElement("span");
					result = document.createElement("input");
					result.style.width = "100%";
					result.disabled = true;
					if (layout === "form"){
						this.checkColSpanResponsive();
						colSpan = this.colSpanControlField(this.gridtable[0], this.gridtable[0][k].model.get("type"), k);
						td.className = "col-xs-12 col-sm-"+colSpan+" col-md-"+colSpan+" col-lg-"+colSpan;
					}else{
						if (layout === "static"){
							td.className = "pmdynaform-grid-field-static"
						}else{
							td.style.width = this.colSpanControlFieldResponsive(this.gridtable[0])+"%";
							td.style.display = "inline-block";
						}
					}
					operation = this.gridtable[0][k].model.attributes.operation;
					if (operation) {
						$(td).addClass("total");
						icon =  iconTotal[operation] ? iconTotal[operation] : iconTotal["other"];
						label.innerHTML = icon + ": ";
						result.value =  title;
						result.id = (operation +"-"+ this.model.get("name")+"-"+
									this.gridtable[0][k].model.get("columnName")).toLowerCase();
						td.appendChild(label);
						td.appendChild(result);
					} else {
						label.innerHTML	= "";
						result.value =  "";
					}

					dom.append(td);
				}
			}

			return this;
		},
		createHTMLContainer: function () {
			var k,
			dom = this.$el.find(".pmdynaform-grid-tbody"),
			pageSize = this.model.get("pageSize"),
			domCarousel,
			flagRow = 0,
			section = 1;

			this.verifyPageNumber();
			dom.children().remove();

			//Applying overflow:auto to container
			if (this.model.get("layout") === "static") {
				dom.addClass("pmdynaform-static");
			}
			

			for (k=0; k<this.dom.length; k+=1) {
				flagRow+=1;	
				if (this.model.get("pager")) {
					if (this.block === true) {
						domCarousel = document.createElement("div");
						domCarousel.className = "pmdynaform-grid-section_"+section;
						if (this.model.get("layout") === "static") {
							domCarousel.className += " pmdynaform-static";
						}
						domCarousel = $(domCarousel);
					} else {
						domCarousel = this.$el.find(".pmdynaform-grid-section_"+section);
					}

					if (section === this.showPage) {
						domCarousel.addClass("item active");
					} else {
						domCarousel.addClass("item");
					}
					if (flagRow === pageSize) {
						this.block = true;
						section+=1;
						flagRow = 0;
					} else {
						this.block = false;
					}
					
					domCarousel.append(this.dom[k]);
					dom.append(domCarousel);
				} else {

					dom.append(this.dom[k]);	
				}
			}
			
			return this;
		},
		createHTMLFields: function (numberRow) {
			var tr, 
			td,
			k,
			tdRemove,
			tdNumber,
			dom,
			element,
			domCarousel,
			colSpan;
			tr = document.createElement("div");
			tr.className = "pmdynaform-grid-row row form-group show-grid";
			if (this.model.get("layout") === "static") {
				tr.className += " pmdynaform-grid-static"
			}
			//xxxxx
			//tr.id = PMDynaform.core.Utils.generateID();
			tdNumber = this.createRowNumber(numberRow+1);
			$(tdNumber).addClass("index-row");
			tr.appendChild(tdNumber);
			for (k=0; k < this.gridtable[numberRow].length; k+=1) {
				colSpan = this.gridtable[numberRow][k].model.get("colSpan");
				td = document.createElement("div");
				if (this.model.get("layout") === "form") {
					if (this.gridtable[numberRow][k].model.get("type")!=="hidden") {
						this.checkColSpanResponsive();
						colSpan = this.colSpanControlField(this.gridtable[numberRow], this.gridtable[numberRow][k].model.get("type"), k);
						td.className = "col-xs-12 col-sm-"+colSpan+" col-md-"+colSpan+" col-lg-"+colSpan;
					}else{
						jQuery(td).css({
							width : 0+"%",
							display : "inline-block" 
						});
					}
				} else if(this.model.get("layout") === "static") {
					if (this.gridtable[numberRow][k].model.get("type")!=="hidden") {
						td.className = "pmdynaform-grid-field-static";
					}
				} else {
					if (this.gridtable[numberRow][k].model.get("type") !== "hidden") {
						td.className = "col-xs-"+colSpan+" col-sm-"+colSpan+" col-md-"+colSpan+" col-lg-"+colSpan;
						jQuery(td).css({
							width : this.colSpanControlFieldResponsive(this.gridtable[numberRow])+"%",
							display : "inline-block" 
						});
					}else{
						jQuery(td).css({
							width : 0+"%",
							display : "inline-block" 
						});
					}
				}
				if (this.gridtable[numberRow][k].el.children.length){
					element = this.gridtable[numberRow][k].el;
				} else {
					element = this.gridtable[numberRow][k].render().el;	
				}
				$(element).addClass("row form-group");
				td.appendChild(element);
				tr.appendChild(td);
			}
			if (this.model.get("mode") !== "view" && this.model.get("mode") !== "disabled") {
				if(this.model.get("deleteRow")){
					tdRemove = this.createRemoveButton(numberRow);
					$(tdRemove).addClass("remove-row");
					tr.appendChild(tdRemove);
				}
			}
			if (this.model.get("layout") === "responsive"){
				jQuery(tdNumber).css({width:this.indexResponsive});
				jQuery(tdRemove).css({width:this.removeResponsive});
			}
			this.dom.push(tr);
			return tr;
		},
		setData: function (data) {
            var col,
            i,
            j,
            cloneData = data,
            grid = this.gridtable;

            if (typeof data === "object") {
                for (j in cloneData) {
                    if(cloneData.hasOwnProperty(j)) {
                    	for (col=0; col<grid[0].length; col+=1) {
                    		if (!_.isEmpty(grid[0][col].model.attributes.variable)) {
                    			if (grid[0][col].model.attributes.variable.var_name === j) {
	                    			if (cloneData[j] instanceof Array) {
	                    				for (i=0; i<grid.length;i+=1) {

	                    					if (!this.gridtable[i][col].model.get("formulator")){
	                    						grid[i][col].model.set("value", cloneData[j][i]);
	                    						if (this.gridtable[i][col].onFieldAssociatedHandler){
	                    							this.gridtable[i][col].onFieldAssociatedHandler()
	                    						}
	                    					}
	                    				}
	                    			}	
	                    		}	
                    		}
                    		
                    	}
                    }
                }
                
            } else {
                //console.log("Error, The 'data' parameter is not valid. Must be an array.");
            }
            return this;
        },
		getData: function () {
			//console.log("getdata grid");
			var i, 
			k, 
			gridpanel,
			fields,
			rowData = [],
			gridData = [],
			gridFieldData = {
				name: this.model.get("name"),
				gridtable: []
			},
			data = this.model.getData();

			gridpanel = this.gridtable;
			for (i=0; i<gridpanel.length; i+=1) {
				rowData = [];
				for (k=0; k<gridpanel[i].length; k+=1) {
					if ((typeof gridpanel[i][k].getData === "function") && 
                        (gridpanel[i][k] instanceof PMDynaform.view.Field)) {
						rowData.push(gridpanel[i][k].getData());
					}
				}
				gridData.push(rowData);
			}
			gridFieldData.gridtable = gridData;

            return gridFieldData;
		},
		renderGridTable: function (isNew) {
			var j, k, row, dom = this.$el.find(".pmdynaform-grid-tbody");

				this.dom = [];
				for(j = 0; j<this.gridtable.length; j+=1){
					this.createHTMLFields(j);
	            }
				this.createHTMLContainer();
			/*} else {
				console.log("new");
				row = this.createHTMLFields(this.model.attributes.rows-1);
                dom.append(row);
			}*/
			this.model.setPaginationItems();
			this.createHTMLPager();
			this.createHTMLTotal();
			return this;
		},
		/**
		 * @Event
		 * @param Event  This must be an event valid
		 * @param Function Callback for the event
		 **/
		on: function (e, fn) {
			var allowEvents = {
				remove: "setOnRemoveRowCallback",
				add: "setOnAddRowCallback",
				pager: "setOnClickPageCallback"
			};

			if (allowEvents[e]) {
				this[allowEvents[e]](fn);
			} else {
				throw new Error ("The event must be a valid event.\n The events available are remove, add and pager");
			}

			return this; 
		},
		setOnRemoveRowCallback: function (fn){
			if (typeof fn === "function") {
				this.onRemoveRowCallback = fn;
			} else {
				throw new Error ("The callback must be a function");
			}
			
			return this;
		},
		setOnAddRowCallback: function(fn){
			if (typeof fn === "function") {
				this.onAddRowCallback = fn;
			} else {
				throw new Error ("The callback must be a function");
			}
			
			return this;
		},
		setOnClickPageCallback: function(fn){
			if (typeof fn === "function") {
				this.onClickPageCallback = fn;
			} else {
				throw new Error ("The callback must be a function");
			}
			
			return this;
		},
		render: function() {
			var j,
			headerGrid,
			bodyGrid;

			this.$el.html( this.template( this.model.toJSON()) );
			this.createHTMLTitle();
			this.renderGridTable();
            //if (this.model.get("hint") !== "") {
                this.enableTooltip();
            //}

            if (this.model.get("layout") === "static") {
            	headerGrid = this.$el.find(".pmdynaform-grid-thead");
				bodyGrid = this.$el.find(".pmdynaform-grid-tbody");
				bodyGrid.css("overflow","auto");
				bodyGrid.scroll(function (event) { 
			        headerGrid.scrollLeft(bodyGrid.scrollLeft());
			        event.stopPropagation();
			    });
            }
            if(!this.model.get("addRow")) {
            	this.$el.find(".pmdynaform-grid-new").hide();
            }
            if (this.model.get("layout") === "responsive") {
            	var size = {
            		"1200": 5,
            		"992": 4,
            		"768": 3,
            		"767": 2
            	};
            
	            $( window ).resize(function() {
	            	var j, 
	            	k,
	            	width = $( window ).width();

	            	if ( width >= 1200) {
	            		//console.log("1200");
	            	}
	            	if (width >= 992 && width < 1200) {
	            		//console.log("992");
	            	}
	            	if (width >= 768 && width < 992) {
	            		//console.log(">768");
	            	}
	            	if (width < 768) {
	            		//console.log("<768");
	            	}

				});
			}
			
			return this;
		},
		afterRender: function () {
			
		},
		getData2: function () {
			var data, gridpanel, i, k, rowData, dataCell,key;
			data = {};

			gridpanel = this.gridtable;
			for (i=0; i<gridpanel.length; i+=1) {
				data[i+1] = {};
				rowData = {};
				for (k=0; k<gridpanel[i].length; k+=1) {
					if ((typeof gridpanel[i][k].getData === "function") && 
                        (gridpanel[i][k] instanceof PMDynaform.view.Field)) {
						dataCell = gridpanel[i][k].model.getData();
						rowData[dataCell.name] = dataCell.value; 
					}
				}
				data[i+1] = rowData;
			}
            return data;
		},
		setData2: function (data) {
           	var rowIndex, grid, dataRow, 
           		colIndexm, cols, colIndex,
           		cellModelItem, cellViewItem,
           		modeItem, dataItem, newItem, value, richi, option, options, i;
           	grid = this.gridtable;
           	for ( rowIndex in data) {
           		if ( parseInt(rowIndex,10) > this.gridtable.length ){
					newItem = this.addRow();
					this.renderGridTable();
					this.onAddRowCallback(newItem, this);
           		}
       			cols = grid[parseInt(rowIndex,10)-1].length;
       			for ( colIndex = 0 ; colIndex < cols ; colIndex +=1 ) {
       				cellViewItem = grid[parseInt(rowIndex,10)-1][colIndex];
       				cellModelItem = grid[parseInt(rowIndex,10)-1][colIndex].model;
       				modeItem = cellModelItem.get("mode");
   					for ( dataItem in data[rowIndex] ) {
   						if (cellModelItem.get("columnName") === dataItem) {
							if ( modeItem === "edit" || modeItem === "disabled" ) {
	       						if (cellModelItem.get("type") === "suggest"){

	                           		for ( richi = 0 ; richi < cellModelItem.get("localOptions").length ; richi +=1 ) {
		                                option  = cellModelItem.get("localOptions")[richi].value;
		                                if (option === data[rowIndex][dataItem]){
		                                    value = cellModelItem.get("localOptions")[richi].label;
		                                    break;
		                                }
		                            }
		                            if (value && !value.length){
		                                for ( richi = 0 ; richi < cellModelItem.get("options").length ; richi +=1 ) {
		                                    option  = cellModelItem.get("options")[richi].value;
		                                    if (option === data[rowIndex][dataItem]){
		                                        value = cellModelItem.get("options")[richi].label;
		                                        break;
		                                    }
		                                }
		                            }

	                                $(cellViewItem.el).find(":input").val(value);
	                                cellModelItem.attributes.value = data[rowIndex][dataItem];
	           					}else if (cellModelItem.get("type") === "checkbox"){
	           						options = cellModelItem.get("options");
	           						if (cellModelItem.get("dataType") === "boolean") {
		                                if ( data[cellModelItem.get("name")] === options[0].value ){
		                                    options[1].selected = false;
		                                    options[0].selected = true;
		                                } else {
		                                    delete options[0].selected;
		                                    options[1].selected = true;
		                                    options[0].selected = false;
		                                }	           							
	           						} else {
	           							for ( i = 0 ; i < options.length ; i +=1 ) {
	           								delete options[i].selected;
	           								if (data[rowIndex][dataItem].indexOf(options[i]) > -1 ) {
	           									options[i].selected = true;
	           								}
	           							}
	           						}
	           						cellModelItem.set("options",options);
	           						cellModelItem.initControl();
	           						cellModelItem.set("value", [data[rowIndex][dataItem]])
	           					} else {
									cellModelItem.set("value", data[rowIndex][dataItem]);
	           					}
							}
							if (modeItem === "view") {
								if (cellModelItem.get("originalType") === "checkbox"){
                            		cellModelItem.set("fullOptions", data[rowIndex][dataItem]);
                        		} else if (cellModelItem.get("originalType") === "dropdown" /*||
                                	cellModelItem.get("originalType") === "suggest"*/) {
                        			value = [];
		                            for ( richi = 0 ; richi < cellModelItem.get("localOptions").length ; richi +=1 ) {
		                                option  = cellModelItem.get("localOptions")[richi].value;
		                                if (option === data[rowIndex][dataItem]){
		                                    value.push(cellModelItem.get("localOptions")[richi].label);
		                                    cellModelItem.set("fullOptions", value);
		                                    break;
		                                }
		                            }
		                            if (!value.length){
		                                for ( richi = 0 ; richi < cellModelItem.get("options").length ; richi +=1 ) {
		                                    option  = cellModelItem.get("options")[richi].value;
		                                    if (option === data[rowIndex][dataItem]){
		                                        value.push(cellModelItem.get("options")[richi].label);
		                                        cellModelItem.set("fullOptions", value);
		                                        break;
		                                    }
		                                }
		                            }
                        		} else {
									value = [];
		                            value.push(data[rowIndex][dataItem]);
		                            cellModelItem.set("fullOptions", value);
								}
							}
       					}
					}
       			}
           	}

            return this;
        }
	});

	PMDynaform.extendNamespace("PMDynaform.view.GridPanel",GridView);
}());
