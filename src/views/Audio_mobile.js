(function(){
    var Audio_mobile = PMDynaform.view.Field.extend({
        validator: null,
        template: _.template($("#tpl-Audio_mobile").html()),
        initialize: function (){
            this.model.on("change", this.render, this);
        },
        render: function() {
            this.$el.html( this.template(this.model.toJSON()) );
            return this;
        }
    });
    PMDynaform.extendNamespace("PMDynaform.view.Audio_mobile", Audio_mobile);
}());
