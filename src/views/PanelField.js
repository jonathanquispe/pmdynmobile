(function(){
    var PanelField = PMDynaform.view.Field.extend({
        validator: null,
        template: _.template($("#tpl-panelField").html()),
        initialize: function (){
        },
        render: function() {
            var content, footer;
            this.$el.html( this.template(this.model.toJSON()) );
            content = jQuery(this.model.get("content"));
            if ( content.length && content instanceof jQuery){
                this.$el.find(".panel-body").append(content);
            } else {
                this.$el.find(".panel-body").text(this.model.get("content"));
            }
            footer = jQuery(this.model.get("footerContent"));
            if ( footer.length && footer instanceof jQuery){
                this.$el.find(".panel-footer").append(footer);
            } else {
                this.$el.find(".panel-footer").text(this.model.get("footerContent"));
            }
            return this;
        }
    });
    PMDynaform.extendNamespace("PMDynaform.view.PanelField", PanelField);
}());
