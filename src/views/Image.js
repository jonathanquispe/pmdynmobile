(function(){
	var ImageView = PMDynaform.view.Field.extend({
		template: _.template( $("#tpl-image").html()),
		events: {
	        "keydown": "preventEvents"
	    },
		initialize: function (){
			this.model.on("change", this.render, this);
		},
		preventEvents: function (event) {
            //Validation for the Submit event
            if (event.which === 13) {
                event.preventDefault();
                event.stopPropagation();
            }
            return this;
        },
		render: function() {
			this.$el.html( this.template(this.model.toJSON()) );
            if (this.model.get("hint") !== "") {
                this.enableTooltip();
            }
			return this;
		}
	});

	PMDynaform.extendNamespace("PMDynaform.view.Image", ImageView);
	
}());
