(function(){
	var FileMobile =  PMDynaform.model.Field.extend({
		defaults: {
            autoUpload: false,
			camera: true,
            colSpan: 12,
            disabled: false,
            dnd: false,
            dndMessage: "Drag or choose local files",
            extensions: "pdf, png, jpg, mp3, doc, txt",
            group: "form",
            height: "100%",
            hint: "",
			id: PMDynaform.core.Utils.generateID(),
            items: [],
            label: "Untitled label",
            labelButton: "Choose Files",
            mode: "edit",
            multiple: false,
            name: PMDynaform.core.Utils.generateName("file"),
            preview: false,
            required: false,
            size: 1, //1 MB
            type: "file",
            proxy: [],
            valid: true,
            validator: null,
            value: "",
            files:[]   
            /**format a file in mobile
                file :{
                    id: ""
                    filePath : ""
                    base64 : ""
                }
                or                
                file : ""
                or mixed

            */
        },

        initialize: function() {
            this.attributes.files= [];
            this.initControl();
            this.set("items", []);
            this.set("proxy", []);
        },
        initControl: function() {
            if (this.get("dnd")) {
                this.set("preview", true);
            }
            return this;
        },
        getData: function() {
            var respData=[];
            for (var i = 0;i< this.attributes.files.length ;i++){
                if(this.attributes.files[i].id){
                    respData.push(this.attributes.files[i].id);
                }else{
                    respData.push(this.attributes.files[i]);
                }
            }
            return {
                name: this.get("name"),
                value: respData
            };
        },
        getDataComplete: function() {
            var respData=[];
            for (var i = 0;i< this.attributes.files.length ;i++){
                if(this.attributes.files[i]["base64"]){
                    respData.push(this.attributes.files[i].id);
                }else{
                    respData.push(this.attributes.files[i]);
                }                
            }
            return {
                name: this.get("name"),
                value: respData
            };
        },
        validate: function (attrs) {
            
        },
        getIDImage: function (index){
           return this.attributes.images[index].id;   
        },
        getBase64Image: function (index){
           return this.attributes.images[index].value;   
        },
        makeBase64Image : function (base64){
            return "data:image/png;base64,"+base64;
        },        
        remoteProxyData : function (arrayImages){           
                var prj = this.get("project"),
                url,
                restClient,
                that = this,
                endpoint,
                that= this,
                respData;
                data = this.formatArrayImagesToSend(arrayImages);
                endpoint = prj.getFullEndPoint(prj.endPointsPath.imageInfo);
                url = prj.getFullURL(endpoint);
                restClient = new PMDynaform.core.Proxy ({
                    url: url,
                    method: 'POST',
                    data: data,
                    keys: prj.token,
                    successCallback: function (xhr, response) {
                        respData = response;
                    }
                });
                respData = this.formatArrayImages(respData);                
                return respData;            
        },
        formatArrayImagesToSend : function (arrayImages){
            var imageId,
                dataToSend = [],
                item = {};

            for (var i = 0; i< arrayImages.length ; i++){
                imageId = arrayImages[i];
                item = {
                    fileId: imageId,
                    width : "100",
                    version :1
                };
                dataToSend.push(item);
            }
            return dataToSend;
        },
        formatArrayImages : function (arrayImages){
            var itemReceive,
                dataToSend = [],
                item = {};

            for (var i = 0; i< arrayImages.length ; i++){
                imageReceive = arrayImages[i];
                item = {
                    id: imageReceive.fileId,
                    base64: imageReceive.fileContent
                };
                dataToSend.push(item);
            }
            return dataToSend;
        },
        remoteProxyDataMedia : function (id){
                var prj = this.get("project"),
                url,
                restClient,
                that = this,
                endpoint,
                that= this,
                respData;                
                endpoint = this.getEndpointVariables({
                    type: "fileStreaming",
                    keys: {
                        "{fileId}": id,
                        "{caseID}": prj.keys.caseID                            
                    }
                });                
                url = prj.getFullURL(endpoint);              
                return url;            
        },
        urlFileStreaming : function (id){
            var prj = this.get("project"),
                url,
                that = this,
                endpoint,                
                dataToSend;                
                endpoint = this.getEndpointVariables({
                    type: "fileStreaming",
                    keys: {
                        "{fileId}": id,
                        "{caseID}": prj.keys.caseID                            
                    }
                });                
                url = prj.getFullURLStreaming(endpoint);
                dataToSend = {
                    id:id,
                    filePath: url
                };
                return dataToSend;
        },        
        getEndpointVariables: function (urlObj) {
            var prj = this.get("project"),
            endPointFixed,
            variable,
            endpoint;

            if (prj.endPointsPath[urlObj.type]) {
                endpoint = prj.endPointsPath[urlObj.type]
                for (variable in urlObj.keys) {
                    if (urlObj.keys.hasOwnProperty(variable)) {
                        endPointFixed =endpoint.replace(new RegExp(variable, "g"), urlObj.keys[variable]);  
                        endpoint= endPointFixed;
                    }
                }
            }

            return endPointFixed;
        }
	});

	PMDynaform.extendNamespace("PMDynaform.model.FileMobile", FileMobile);
}());