(function(){
    var GeoMobile =  PMDynaform.model.Field.extend({
        defaults: {
            id: PMDynaform.core.Utils.generateID(),
            type: "location",
            label: "Untitled label",
            mode: "edit",
            group: "form",
            labelButton: "Map",
            name: "name",
            colSpan: 12,
            height: "auto",            
            value: "",
            required: false,
            hint: "",
            disabled: false,
            preview: false,
            valid: true,

            geoData:null,
            interactive: true            
        },
        initialize: function() {
            this.initControl();
        },
        initControl: function() {
            this.attributes.images = [];
            //if (this.get("dnd")) {
                this.set("preview", true);
            //}
            return this;
        },
        isValid: function() {
            this.set("valid", this.get("validator").get("valid"));
            return this.get("valid");
        },
        getDataRFC: function() {
            var geoValue = this.attributes.geoData;
            if(geoValue == "null" || geoValue == null){
                geoValue=null;                    
            }else{
                if(geoValue.imageId == "" || geoValue.imageId == null || typeof geoValue.imageId == "undefined"){
                    geoValue = geoValue;
                }
            }
            if(geoValue){
                if(geoValue.data){
                    delete geoValue.data;
                }
            }
            return {
                name: this.get("name"),
                value: geoValue
            };
        },
        getData: function() {
            var geoValue = this.attributes.geoData;            
            if(geoValue){
                if(geoValue.base64){
                    delete geoValue.base64;
                }
            }
            return {
                name: this.get("name"),
                value: geoValue
            };
        },
        
        validate: function (attrs) {
            
        },        
        remoteProxyData : function (id){           
            var prj = this.get("project"),
            url,
            restClient,
            that = this,
            endpoint,
            that= this,
            respData;            
            endpoint = this.getEndpointVariables({
                        type: "getImageGeo",
                        keys: {                           
                            "{caseID}": prj.keys.caseID,                                                           
                        }
                    });
                    url = prj.getFullURL(endpoint);
            url = prj.getFullURL(endpoint);
            restClient = new PMDynaform.core.Proxy ({
                url: url,
                method: 'POST',
                data:[{
                    fileId: id,
                    width : "600",
                    version :1
                }],                
                keys: prj.token,
                successCallback: function (xhr, response) {
                    respData = response;
                }
            });
            //this.set("proxy", restClient);
            return {
                id: respData[0].fileId,
                base64: respData[0].fileContent
            };
            
        },
        getImagesNetwork : function (location){           
            var prj = this.get("project"),
            url,
            restClient,
            that = this,
            endpoint,
            that= this,
            respData;            
            endpoint = this.getEndpointVariables({
                        type: "getImageGeo",
                        keys: {
                            "{fileID}": location.imageId,
                            "{caseID}": prj.keys.caseID,                                                           
                        }
                    });
                    url = prj.getFullURL(endpoint);
            url = prj.getFullURL(endpoint);            
            restClient = new PMDynaform.core.Proxy ({
                url: url,
                method: 'POST',
                data: {
                    fileId: location.imageId,
                    width : "600",
                    version :1
                },                
                keys: prj.token,
                successCallback: function (xhr, response) {
                    respData = response;
                }
            });
            this.set("proxy", restClient);
            return respData;            
        },        
        remoteGenerateLocation : function (location){           
            var prj = this.get("project"),
            url,
            restClient,
            that = this,
            endpoint,
            that= this,
            dataToSend = new FormData(),
            respData;           
                        
            dataToSend.append("tas_uid",location);                        
            dataToSend.append("tas_uid",prj.keys.taskID);
            dataToSend.append("app_doc_comment","");
            dataToSend.append("latitude",location.latitude);
            dataToSend.append("longitude",location.longitude);
     
            endpoint = this.getEndpointVariable({
                        type: "generateImageGeo",
                        keys: {
                            "{caseID}": prj.keys.caseID                          
                        }
                    });
                    url = prj.getFullURL(endpoint);
            url = prj.getFullURL(endpoint);

            restClient = new PMDynaform.core.Proxy ({
                url: url,
                method: 'POST',
                data:dataToSend,                
                keys: prj.token,
                successCallback: function (xhr, response) {
                    respData = response;
                }
            });
            this.set("proxy", restClient);
            return respData;
            
        },
        getEndpointVariables: function (urlObj) {
            var prj = this.get("project"),
            endPointFixed,
            variable,
            endpoint;

            if (prj.endPointsPath[urlObj.type]) {
                endpoint = prj.endPointsPath[urlObj.type]
                for (variable in urlObj.keys) {
                    if (urlObj.keys.hasOwnProperty(variable)) {
                        endPointFixed =endpoint.replace(new RegExp(variable, "g"), urlObj.keys[variable]);  
                        endpoint= endPointFixed;
                    }
                }
            }

            return endPointFixed;
        }   
    });

    PMDynaform.extendNamespace("PMDynaform.model.GeoMobile", GeoMobile);
}());