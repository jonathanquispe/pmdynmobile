(function(){
    var Signature_mobile =  PMDynaform.model.Field.extend({
        defaults: {
            id: PMDynaform.core.Utils.generateID(),
            type: "signature",
            label: "Untitled label",
            mode: "edit",
            group: "form",
            labelButton: "Signature",
            name: "name",
            colSpan: 12,
            height: "auto",            
            value: "",
            required: false,
            hint: "",
            disabled: false,
            preview: false,
            valid: true,
            files:[]            
        },
        initialize: function() {
            this.initControl();
        },
        initControl: function() {
            this.attributes.files= [];
            this.set("preview", true);            
            return this;
        },
        isValid: function() {
            this.set("valid", this.get("validator").get("valid"));
            return this.get("valid");
        },
        getData: function() {
            var i,
                response = [],
                signatureValue = this.attributes.files;
            for (i = 0;i< signatureValue.length;i++){
                if(typeof signatureValue[i].id  != "undefined" && signatureValue[i].id != null){
                    response.push(signatureValue[i].id);
                }
            }
            return {
                name: this.get("name"),
                value: response
            };             
        },
        getDataCustom: function() {
            var signatureValue = this.attributes.files;            
            return {
                name: this.get("name"),
                value: signatureValue
            };
        },
        validate: function (attrs) {
            
        },        
        remoteProxyData : function (id){           
            var prj = this.get("project"),
            url,
            restClient,
            that = this,
            endpoint,
            that= this,
            respData;            
            endpoint = this.getEndpointVariables({
                        type: "getImageGeo",
                        keys: {                           
                            "{caseID}": prj.keys.caseID,                                                           
                        }
                    });
            url = prj.getFullURL(endpoint);
            restClient = new PMDynaform.core.Proxy ({
                url: url,
                method: 'POST',
                data:[{
                    fileId: id,
                    width : "300",
                    version :1
                }],                
                keys: prj.token,
                successCallback: function (xhr, response) {
                    respData = response;
                }
            });
            return {
                id: respData[0].fileId,
                base64: respData[0].fileContent
            };            
        },
        remoteGenerateID : function (location){           
            var prj = this.get("project"),
            url,
            restClient,
            that = this,
            endpoint,
            that= this,
            respData;            
            endpoint = this.getEndpointVariable({
                        type: "generateImageGeo",
                        keys: {
                            "{caseID}": prj.keys.caseID                          
                        }
                    });
                    url = prj.getFullURL(endpoint);
            url = prj.getFullURL(endpoint);
            restClient = new PMDynaform.core.Proxy ({
                url: url,
                method: 'POST',
                data:location,                
                keys: prj.token,
                successCallback: function (xhr, response) {
                    respData = response;
                }
            });
            this.set("proxy", restClient);
            return respData;
            
        },
        getEndpointVariables: function (urlObj) {
            var prj = this.get("project"),
            endPointFixed,
            variable,
            endpoint;

            if (prj.endPointsPath[urlObj.type]) {
                endpoint = prj.endPointsPath[urlObj.type]
                for (variable in urlObj.keys) {
                    if (urlObj.keys.hasOwnProperty(variable)) {
                        endPointFixed =endpoint.replace(new RegExp(variable, "g"), urlObj.keys[variable]);  
                        endpoint= endPointFixed;
                    }
                }
            }

            return endPointFixed;
        }   
    });

    PMDynaform.extendNamespace("PMDynaform.model.Signature_mobile", Signature_mobile);
}());