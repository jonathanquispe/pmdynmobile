(function(){
    var FileMobile =  PMDynaform.model.FieldMobile.extend({
        defaults: {
            id: PMDynaform.core.Utils.generateID(),
            type: "file",
            label: "Untitled label",
            mode: "edit",
            group: "form",
            labelButton: "Choose Files",
            name: "name",
            colSpan: 12,
            height: "auto",
            dnd: false,
            value: "",
            required: false,
            hint: "",
            disabled: false,
            preview: false,
            multiple: false,
            validator: null,
            valid: true, 
            images: [],
            thumbnails: []
        },
        initialize: function() {
            this.initControl();
        },
        initControl: function() {
            this.attributes.images = [];
            if (this.get("dnd")) {
                this.set("preview", true);
            }
            return this;
        },
        isValid: function() {
            this.set("valid", this.get("validator").get("valid"));
            return this.get("valid");
        },
        getData: function() {
            return {
                name: this.get("name"),
                value: this.get("value")
            };
        },
        validate: function (attrs) {
            
        },
        getIDImage: function (index){
           return this.attributes.images[index].id;   
        },
        getBase64Image: function (index){
           return this.attributes.images[index].value;   
        },
        makeBase64Image : function (base64){
            return "data:image/png;base64,"+base64;
        },
        getData: function() {
            var respData=[];
            for (var i = 0;i< this.attributes.images.length ;i++){
                respData.push(this.attributes.images[i].id);
            }
            return {
                name: this.get("name"),
                value: respData
            };
        },
        remoteProxyData : function (arrayImages){
            var prj = this.get("project"),
            url,
            restClient,
            that = this,
            endpoint,
            that= this,
            respData;
            data = arrayImages;
            endpoint = prj.getFullEndPoint(prj.endPointsPath.imageInfo);
            url = prj.getFullURL(endpoint);
            restClient = new PMDynaform.core.Proxy ({
                url: url,
                method: 'POST',
                data: data,
                keys: prj.token,
                successCallback: function (xhr, response) {
                    respData = response.data;
                }
            });
            this.set("proxy", restClient);
            return respData;
        },
        remoteProxyImage : function (id){
            var prj = this.get("project"),
            url,
            restClient,
            that = this,
            endpoint,
            that= this,
            respData;            
            endpoint = this.getEndpointVariable({
                        type: "imageDownload",
                        keys: {
                            "{imageID}": id,
                            "{caseID}":prj.keys.caseID
                        }
                    });
                    url = prj.getFullURL(endpoint);
            url = prj.getFullURL(endpoint);
            restClient = new PMDynaform.core.Proxy ({
                url: url,
                method: 'GET',                
                keys: prj.token,
                successCallback: function (xhr, response) {
                    respData = response;
                }
            });
            this.set("proxy", restClient);
            return respData;
        }      
    });

    PMDynaform.extendNamespace("PMDynaform.model.FileMobile", FileMobile);
}());