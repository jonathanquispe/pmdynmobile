(function(){
	var ProjectFlowIOS = function(options) {
		this.project = null;
		this.nextForm = false;
		this.sendFormData = null;
 		this.forms = null;
 		this.indexForms = 0;
		this.currentFlow = null;
		this.onLine = true;
		this.isCase = false;					
	};

	ProjectFlowIOS.prototype = new PMDynaform.core.ProjectFlow;

	ProjectFlowIOS.prototype.getIndexForms = function(forms) {
		return this.indexForms;		
	};

	ProjectFlowIOS.prototype.setIsCase = function(isCase) {		
		this.isCase = isCase;
		return this;		
	};	

	ProjectFlowIOS.prototype.setForms = function(forms) {
		this.forms = forms;		
	};
	
	ProjectFlowIOS.prototype.initFlow = function(project, onLine) {
		this.project = project;
		this.onLine = onLine;		
	};


	ProjectFlowIOS.prototype.initLoad = function(data) {
		var sw = null, 
			response, 
			jsonForm,
			dataForm,
			allData,
			project = this.project,
			formsHandler = this.project.formsHandler;
			dataManager = this.project.dataManager;
		if(project.caseID==null){
			this.submitNew();
			this.executeFakeIOS("start-case");					
		}

		this.executeTriggerBefore();

		if(this.project.dataForm == null || typeof this.project.dataForm == "undefined"){
			//if(this.isCase){
				allData = project.loadAllDataCase();
				if(allData.state == "success"){					
					dataManager.addData(allData.data);
					dataManager.addDataLocal(allData.data);					
				}else{
					this.setToastMessage(project.language["ERROR_NETWORK_FORM_DATA"]);																		
				}
			//}
		}
			 
		if(data.jsonForm != null){			
			this.project.loadProject(data.jsonForm);
			formsHandler.setJsonFormCurrent(data.jsonForm);				
			//dataForm = formsHandler.getDataForm();
			dataForm = dataManager.getDataLocal();
			if(dataForm != null ){
				this.project.setDataViewDelay(dataForm);
			}
			return;				
		}		
		if(data.jsonForm == null){
			jsonForm = formsHandler.getJsonFormCurrentNodevice();
			if(jsonForm != null){
				this.project.loadProject(jsonForm);				
				formsHandler.setJsonFormCurrent(jsonForm);
				dataForm = dataManager.getDataLocal();				
				//dataForm = formsHandler.getDataForm();
				if(dataForm != null){
					this.project.setDataViewDelay(dataForm);
				}
				return;
			}
		}			
	};

	ProjectFlowIOS.prototype.submitStep = function(data) {		
		var sw = null, 
			response,
			formsHandler = this.project.formsHandler,
			dataManager = this.project.dataManager,
			project = this.project,
			indexCurrent;					

		dataManager.addData(data.dataSubmit);
		dataManager.addDataLocal(data.dataSubmit);	
		if(data.dataSubmit){
			if(this.project.typeList ==null){
				indexCurrent = formsHandler.getIndexForms();				
				if(project.caseID){					
					this.submitCase(data);					
				}
				else{
					if(project.caseID==null){
						this.submitNew();
						this.submitCase(data);
					}
				}
			}else{				
				this.submitCase(data);
			}
		}
		project.showNavBar();				
	};

	ProjectFlowIOS.prototype.submitNew = function() {
		var sw = null, 
			response, 
			infocase,
			jsonForm,
			dataForm,
			route,
			formsHandler = this.project.formsHandler,
			project =this.project;

			infoCase=this.project.startCase();
			if(infoCase.state=="success"){					
				this.project.setCaseID(infoCase.caseID);
				this.project.caseTitle=infoCase.caseTitle;
				this.project.caseNumber=infoCase.caseNumber;				
		   		return;
			}
			if(infoCase.state == "internetFail"){								
			    this.setToastMessage("Connection failed in submit new form");													
				this.internetFailData();				
				return;				
			}

			if(infoCase.state == "unautorized"){				
				this.setToastMessage("Connection failed in submit new form");													
				this.internetFailData();
				return;
			}						
	};

	ProjectFlowIOS.prototype.nextStepCaseIOS = function(formID, jsonFormIOS) {		
		return;
	};

	ProjectFlowIOS.prototype.submitCase = function(data) {		
			var route,
				dataForm,
				project = this.project,
				formsHandler = this.project.formsHandler,
				dataManager = this.project.dataManager;
							
			if(data.dataSubmit){
				caseID=this.project.submitFormCase(data.dataSubmit);				
				if(caseID.state=="success"){
					this.executeTriggerAfter();										
					this.sendSubmitInformationDevice();
					this.setToastMessage(project.language["INFO_PMDYNAFORM_SUBMIT"]);	
					if(formsHandler.existNextForm()){
						formsHandler.advanceForm();
						project.emptyView();
						jsonForm = formsHandler.getJsonFormCurrentLocal();
						if(jsonForm != null){							
							project.loadProject(jsonForm);
							//dataForm = formsHandler.getDataForm();
							dataForm = dataManager.getDataLocal();
							if(dataForm!=null){
								project.setDataViewDelay(dataForm);
							} 
						}else{
							this.executeFakeIOS("next-form");
							return;
						}
						return;
					}
					else{
						this.submitRoute();
					}					
					return;
				}				
				if(caseID.state == "internetFail"){
					this.setToastMessage("Error");
					this.internetFailData();
				}					
				if(caseID.state == "unautorized"){
					this.setToastMessage("Error");
					this.internetFailData();
				}
			}				
	};


	ProjectFlowIOS.prototype.submitRoute = function() {		
			var route,
				dataForm,
				str = "",
				project = this.project,
				formsHandler = this.project.formsHandler;
				
			route = this.project.routeCase();			
			if(route.state == "success"){
				if(route.data.routing.length >0){
					str = route.data.routing[0].userName;				
					str=project.language["INFO_PMDYNAFORM_DERIVATED"]+"\n Next User:"+str;								
				}else{
					str = route.data.routing.message;				
					str= "Message:"+str;								
				}				
				this.setToastMessage(str);
				this.closeWebViewDerivated();
		   		return;
			}
			if(route.state == "internetFail"){
				this.setToastMessage("Error in route");
				this.closeWebViewInternetFailed();
			}
			if(route.state == "unautorized"){
				this.setToastMessage("Error in route");
				this.closeWebViewInternetFailed();
			}							
	};

	ProjectFlowIOS.prototype.advanceStep = function() {
		var response=null,
		    formID,
		    dataForm,
		    jsonForm,
		    formsHandler = this.project.formsHandler,
		    dataManager = this.project.dataManager,		    
		    project= this.project;

		this.executeTriggerAfter();    
		if(formsHandler.existNextForm()){
			formsHandler.advanceForm();			
			jsonForm = formsHandler.getJsonFormCurrentLocal();
			if(jsonForm == null){			
				this.executeFakeIOS("next-form");
				//this.executePrevOrNextStepIOS("formID",null);
				return;
			}
			else{
				this.executeTriggerBefore();
				project.loadProject(jsonForm);
				//dataForm = formsHandler.getDataForm();
				allData = project.loadAllDataCase();
				if(allData.state == "success"){										
					dataManager.addData(allData.data);
					dataManager.addDataLocal(allData.data);											
				}else{
					this.setToastMessage(project.language["ERROR_NETWORK_FORM_DATA"]);													
					this.internetFailData();
				}
				dataForm = dataManager.getDataLocal();
				project.setDataViewDelay(dataForm);
			}

		}else{
			return null;
		}		
	};


	ProjectFlowIOS.prototype.backStep = function() {
		var response=null,
		    formID,
		    dataForm,
		    jsonForm,
		    formsHandler = this.project.formsHandler,
		    dataManager = this.project.dataManager,		    
		    project= this.project;

		if(formsHandler.existPrevForm()){
			formsHandler.backForm();						
			jsonForm = formsHandler.getJsonFormCurrentLocal();
			if(jsonForm == null){			
				this.executeFakeIOS("prev-form");
				//this.executePrevOrNextStepIOS("formID",null);
				return;
			}
			else{
				this.executeTriggerBefore();
				project.loadProject(jsonForm);
				//dataForm = formsHandler.getDataForm();
				allData = project.loadAllDataCase();
				if(allData.state == "success"){										
					dataManager.addData(allData.data);
					dataManager.addDataLocal(allData.data);											
				}else{
					this.setToastMessage(project.language["ERROR_NETWORK_FORM_DATA"]);													
					this.internetFailData();
				}
				dataForm = dataManager.getDataLocal();
				project.setDataViewDelay(dataForm);
			}			
		}else{
			return ;
		}		
	};

	ProjectFlowIOS.prototype.executePrevOrNextStepIOS = function(formID, jsonFormIOS) {		
		var response,
			responseDevice,
			jsonForm,
			indexCurrent=this.indexForms,
			formsHandler= this.project.formsHandler;
			dataManager= this.project.dataManager;			
			project= this.project;		
		// Verify local data json		
		if(jsonFormIOS!=null){
			jsonForm= JSON.parse(jsonFormIOS);
			//formsHandler.setJsonForm(indexCurrent, jsonForm);
			formsHandler.setJsonFormIndexCurrent(jsonForm);
			this.executeTriggerBefore();
			project.loadProject(jsonForm);
			this.loadDataProject();
			dataForm = dataManager.getDataLocal();
			project.setDataViewDelay(dataForm);
			return;
		}else{
			jsonForm=formsHandler.getJsonFormCurrentNetwork();			
			if(jsonForm  != null){
				this.executeTriggerBefore();
				project.loadProject(jsonForm);
				formsHandler.setJsonFormCurrent(jsonForm);			
				//dataForm = formsHandler.getDataForm();
				this.loadDataProject();
				dataForm = dataManager.getDataLocal();
				project.setDataViewDelay(dataForm);
				return;	
			}else{
				this.setToastMessage("Error");
				this.internetFailData();		
			}			  
		}					
		return this;			
	};

	ProjectFlowIOS.prototype.loadDataProject = function() {
		var allData = project.loadAllDataCase(),
			dataManager= this.project.dataManager;	
		if(allData.state == "success"){										
			dataManager.addData(allData.data);
			dataManager.addDataLocal(allData.data);											
		}else{
			this.setToastMessage(project.language["ERROR_NETWORK_FORM_DATA"]);													
			this.internetFailData();
		}
	};

	ProjectFlowIOS.prototype.internetFailData = function() {
		this.executeFakeIOS("close-webview");	  		
		
	};

	ProjectFlowIOS.prototype.closeWebViewDerivated = function() {		
		this.executeFakeIOS("derivated");
					
	};

    ProjectFlowIOS.prototype.getJsonFormFromDevice = function() {		
		switch(this.project.userAgent) {
			case "ios":
				this.executeFakeIOS("next-form");
			    break;    	
			default:
			    break;
		}
		return null;	
	};

	ProjectFlowIOS.prototype.setToastMessage= function (message) { 
    	if(navigator.userAgent==="formslider-ios"){    			    						
			this.message = message;			
		}
		return this;		
    }; 


    ProjectFlowIOS.prototype.setCaseInformationDevice= function () {    	
        this.executeFakeIOS("submit-nextform");												
		return this;			
    };

    ProjectFlowIOS.prototype.sendSubmitInformationDevice= function () {    	
        this.executeFakeIOS("submit-nextform");												
		return this;			
    };
    ProjectFlowIOS.prototype.executeFakeIOS = function(url) {    	
		var iframe = document.createElement("IFRAME");
		iframe.setAttribute("src", "ios:" + url);
		document.documentElement.appendChild(iframe);
		iframe.parentNode.removeChild(iframe);
		iframe = null; 
	};
	PMDynaform.extendNamespace("PMDynaform.core.ProjectFlowIOS", ProjectFlowIOS);
}());