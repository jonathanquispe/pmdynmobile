
(function(){
	var WebServicesManager = function(options) {		
		
		this.webFunctions={
			getJsonForm : "",
			getAllData : "",
			submitNew : "",
			submitCase : "",
			submitRoute : "",
		};
		this.endPointsPath ={			
			jsonForm :"dynaform/{formID}",
			allDataCase :"case/{caseID}/data/all",
			submitNew :"process/{processID}/task/{taskID}/dynaform/{formID}",
			submitCase :"case/{caseID}/dynaform/{formID}/savedata",
			submitRoute :"case/{caseID}/dynaform/{formID}/savedata/route",		
			
			newTokens :"oauth/token",
			caseTypeList :"case/{caseID}/dynaform/{typeList}",
			loadDynaform :"case/{caseID}/dynaform/{formID}/data",
			getFormData :"case/{caseID}/dynaform/{formID}/data",
												
				
			createVariable: "process-variable",
			imageInfo: "case/{caseID}/images/thumbnails",
			imageDownload: "case/{caseID}/downloadfile/{imageID}",
			fileDownload: "case/{caseID}/file/{fileID}",			
			variableList: "process-variable",
			getImageGeo:"image/{fileID}/thumbnails",
			variableInfo: "process-variable/{var_uid}",			
			executeQuery: "process/{processID}/process-variable/{var_name}/execute-query",
			uploadFile: ""
		};			
		WebServicesManager.prototype.init.call(this, options);
	};
	
	WebServicesManager.prototype.init = function(options) {
		var defaults = {
			name: "",
			data: []
		};
		this.viewfields = [];
		$.extend(true, defaults, options);
		this.setServer(defaults.server);
		this.setProcessID(defaults.processID);
		this.setWorkspace(defaults.workspace);		
		this.setTaskID(defaults.taskID);
		this.setCaseFakeID(defaults.caseFakeID);		
		this.setCaseID(defaults.caseID);
		this.setFormID(defaults.formID);
		this.setData(defaults.data);
		this.setDataForm(defaults.dataForm);						
		this.setTypeList(defaults.typeList);				
	    this.setSubmitRest(defaults.submitRest);	    
		this.setOnLine(defaults.onLine);
		this.setDynaforms(defaults.dynaforms);
		this.setContainer(defaults.container);
		this.setLanguage();
		//this.loadMask = new PMDynaform.view.LoadMask();		
	};

	WebServicesManager.prototype.setProxy = function(keys) {
		//usage the dataRestClient
		//processId: "",
        //workspace: "",
        //token: "",
        //apiVersion: '1.0'
		this.keys = keys;	    	
    	this.restClient = new PMDynaform.core.Proxy({				
            method: 'POST',
            keys:{
            	processId: this.processID,
                workspace: this.workspace,
                token: this.tokens                
            },				
            server:this.server            
        });    
	};


	WebServicesManager.prototype.getJsonForm = function() {
		var restClient, 
			endpoint, 
			url, 
			that=this, 
			resp;
		endpoint = this.getFullEndPoint(this.endPointsPath.jsonForm);
        url = this.getFullURL(endpoint);
		restClient = new PMDynaform.core.Proxy ({
	            url: url,
	            method: 'GET',                    
	            keys: this.token,
	            successCallback: function (xhr, response) {
	            resp= {
	            		"data":response.data.formContent,
	            		"state":"success"
	            	}  	                
	            },
	            failureCallback: function (xhr, response) {
	            	resp= {	            	
	            		"state":"internetFail"
	            	}
	           	}
	        });                		
		return resp;
	};


	WebServicesManager.prototype.getAllData = function() {
		var restClient, 
			endpoint, 
			url, 
			that=this, 
			resp;
		endpoint = this.getFullEndPoint(this.endPointsPath.allDataCase);
        url = this.getFullURL(endpoint);		
		restClient = new PMDynaform.core.Proxy ({
            url: url,
            method: 'GET',                    
            keys: this.token,
            successCallback: function (xhr, response) {
            	resp = {
            		"state":"success",
            		"data":response.data
            	};            	            
            },
            failureCallback : function (xhr, response){
            	resp = {
            		"state":"internetFail"
            	};	
			}
        });
		return resp;
	};

	WebServicesManager.prototype.submitNew = function(data) {
		var dataSend={},
			that = this,
			i,
			restClient, 
			endpoint, 
			url,
			arrayDep, 
			self=this, 
			resp,
			submitData;

		for(i=0;i<data.length;i++){
			if(data[i].value){
				dataSend[data[i].name]=data[i].value;
			}
		}

		submitData ={
			data:dataSend,
			caseFakeID:this.caseFakeID?this.caseFakeID:null
		};

		endpoint = that.getFullEndPoint(that.endPointsPath.submitNew);
		url = that.getFullURL(endpoint);
        
		restClient = new PMDynaform.core.Proxy ({
            url: url,
	            method: 'POST',                    
	            keys: that.token,
	            data:submitData,
	            successCallback: function (xhr, response) {
	            	resp = {
	            		"state":"success",
	            		"caseID":response.caseId,
	            		"nextStep":response.nextStep,
	            		"caseTitle":response.caseTitle,
	            		"caseNumber":response.caseNumber
	            	};
	            },
	            failureCallback: function (xhr, response) {            		
					resp= {	            		
	            		"state":"internetFail"
	            	}										 					
	           	}
	        });

		this.caseFakeID = undefined;
	    return resp;					
	};

	WebServicesManager.prototype.submitCase = function(data) {
		var dataSend={},
			that = this,
			i,
			restClient, 
			endpoint, 
			url,
			arrayDep, 
			self=this,
			submitData, 
			resp;

		for(i=0;i<data.length;i++){
			if(data[i].value){
				dataSend[data[i].name]=data[i].value;
			}
		}

		submitData={
			data:dataSend
		};

		endpoint = that.getFullEndPoint(that.endPointsPath.submitCase);
		url = that.getFullURL(endpoint);	        
	       
		restClient = new PMDynaform.core.Proxy ({
            url: url,
	            method: 'POST',                    
	            keys: that.token,
	            data:submitData,
	            successCallback: function (xhr, response) {
	            	resp = {
	            		"state":"success",
	            		"caseID":response.caseId,
	            		"nextStep":response.nextStep
	            	};
	            },
	            failureCallback: function (xhr, response) {            		
					resp= {	            		
	            		"state":"internetFail"
	            	}										 					
	           	}
	        });
	    return resp;					
	};

	WebServicesManager.prototype.submitRoute = function(data) {
		var dataSend={},
			that = this,
			i,
			restClient, 
			endpoint, 
			url,
			arrayDep,
			submitData, 			
			resp;

		for(i=0;i<data.length;i++){
			if(data[i].value){
				dataSend[data[i].name]=data[i].value;
			}
		}
		submitData={
			data:dataSend
		};
		endpoint = that.getFullEndPoint(that.endPointsPath.submitRoute);
        url = that.getFullURL(endpoint);	        
		restClient = new PMDynaform.core.Proxy ({
            url: url,
	            method: 'POST',                    
	            keys: that.token,
	            data:submitData,
	            successCallback: function (xhr, response) {
	            	resp = {
	            		"state":"success",
	            		"response":response.data.flows,
	            		"allData":response
	            	};
	            },
	            failureCallback: function (xhr, response) {            							
					resp= {	            		
	            		"state":"internetFail"
	            	};										 					
	           	}
	        });
	    return resp;					
	};
	PMDynaform.extendNamespace("PMDynaform.core.WebServicesManager", WebServicesManager);
}());