(function(){
	var DropDownViewMobile = PMDynaform.view.Field.extend({
		events: {
			"change select": "continueDependentFields",
            "blur select": "validate",
            "keydown select": "preventEvents"
		},
		clicked: false,	
		template: _.template( $("#tpl-extdropdown").html()),
		initialize: function () {
			this.model.on("change", this.checkBinding, this);
		},
		checkBinding: function (event) {
			if (!this.clicked) {
				this.render();
				this.validate();
			}
			this.onChange(event);
			return this;
		},
		preventEvents: function (event) {
            //Validation for the Submit event
            if (event.which === 13) {
                event.preventDefault();
                event.stopPropagation();
            }
            return this;
        },
		setValueDefault: function(){
			var val = $(this.el).find(":selected").val();
			if(val!= undefined) {
				this.model.set("value",val);
			}
			else {
				this.model.set("value","");	
			}
		},
		onChange: function (event)  {
			var i, j, item, dependents, viewItems, valueSelected;
			
			dependents = this.model.get("dependentFields") ? this.model.get("dependentFields"): [];
			viewItems = this.parent.items.asArray();			
			if (dependents.length > 0) {
				for (i = 0; i < viewItems.length; i+=1) {
					for (j = 0; j < dependents.length; j+=1) {
						item = viewItems[i].model.get("name");	
						if(dependents[j] === item) {
							if (event) {
								if (viewItems[i].onDependentHandler) {
									viewItems[i].onDependentHandler(item,valueSelected);
									viewItems[i].render();
									viewItems[i].setValueDefault();																
								}
							}
						}
					}
				}
			}
			this.clicked = false;

			return this;
		},
		createDependencies : function () {
			var i, j, item, dependents, viewItems;
			dependents = this.model.get("dependentFields") ? this.model.get("dependentFields"): [];
			viewItems = this.parent.items.asArray();			
			if (dependents.length > 0) {
				for (i = 0; i < viewItems.length; i+=1) {
					for (j = 0; j < dependents.length; j+=1) {
						item = viewItems[i].model.get("name");
						if(dependents[j] === item) {
							if (viewItems[i].model.setDependencies) {
								viewItems[i].model.setDependencies(this);
							}
						}
					}
				}
			}

			return this;
		},		
		continueDependentFields: function () {
			var newValue, auxValue;
			this.clicked = true;
			auxValue = $(this.el).find(":selected").val();
			newValue = (auxValue === undefined)? "" : auxValue;			
			this.model.set("value", newValue);		
			return this;
		},
		onDependentHandler: function (name,value) {
			this.model.remoteProxyData(true);
			return this;
		},							
        validate: function(){
        	var drpValue;
        	if(!this.model.get("disabled")) {
        		drpValue = (this.model.get("options").length > 0)? this.$el.find("select").val() : "";
	            this.model.set({value: drpValue}, {validate: true});
	            if (this.validator) {
	                this.validator.$el.remove();
	                this.$el.removeClass('has-error');
	            }
	            if(!this.model.isValid()){
	                this.validator = new PMDynaform.view.Validator({
	                    model: this.model.get("validator")
	                });  
	                this.$el.find("select").parent().append(this.validator.el);
	                this.applyStyleError();
	            }
        	}        	
            return this;
        },        
		render: function() {
			this.createDependencies();
			this.$el.html(this.template(this.model.toJSON()));
			if (this.model.get("hint")) {
				this.enableTooltip();
			}
			return this;
		},
		afterRender : function () {
			this.continueDependentFields();
			return this;
		}
	});

	PMDynaform.extendNamespace("PMDynaform.view.DropdownMobile",DropDownViewMobile);
	
}());
