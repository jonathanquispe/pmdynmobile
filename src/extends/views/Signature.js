(function(){
	var Signature_mobile = PMDynaform.view.Field.extend({
		item: null,	
		template: _.template( $("#tpl-ext-signature").html()),
		templatePlus: _.template( $("#tpl-extfile-plus").html()),						
		viewsImages: [],
		imageOffLine : "geoMap.jpg",		
		events: {
	        "click button": "onClickButton"	        
	    },
		initialize: function () {			
			
		},		
		onClickButton: function (event) {			
			var respData;
			this.model.set("interactive",true);			
			respData = {
					idField: this.model.get("name")					
				};			
			if(navigator.userAgent == "formslider-android"){
				JsInterface.getSignature(JSON.stringify(respData));				
			}
			if(navigator.userAgent == "formslider-ios"){
				this.model.attributes.project.setMemoryStack({"data":respData,"source":"IOS"});
				this.model.attributes.project.projectFlow.executeFakeIOS("signature");
			}
			event.preventDefault();
			event.stopPropagation();			
			return this;
		},
		makeBase64Image : function (base64){
            return "data:image/png;base64,"+base64;
        },		
		createBox: function (data) {
			var rand,
				newsrc,
				template,
				resizeImage,
				preview,
				progress;

			if(data.filePath){
				newsrc = data.filePath;
			}else{
				newsrc = this.makeBase64Image(data.base64); 	    		
			}	
			rand = Math.floor((Math.random()*100000)+3);
	    	
	    	template = document.createElement("div"),
	    	resizeImage = document.createElement("div"),
	    	preview = document.createElement("span"),
	    	progress = document.createElement("div");

	    	template.id = rand;
	    	template.className = "pmdynaform-file-containergeo";

			resizeImage.className = "pmdynaform-file-resizeimage";
			resizeImage.innerHTML = '<img src="'+newsrc+'">';	    	
	    	preview.id = rand;
	    	preview.className = "pmdynaform-file-preview";
			preview.appendChild(resizeImage);
	    	template.appendChild(preview);	    	
	    	this.$el.find(".pmdynaform-ext-geo").prepend(template);	    	
	    	this.hideButton();
	    	return this;
		},
		hideButton : function (){
			var button;
			button = this.$el.find("button");
			button.hide();	    	
		},	
		render: function () {
			var that = this,
				fileContainer,
				fileControl;			
			this.$el.html( this.template(this.model.toJSON()));			
			if (this.model.get("hint")) {
				this.enableTooltip();
			}
			fileContainer = this.$el.find(".pmdynaform-file-droparea-ext")[0];			
			fileControl = this.$el.find("input")[0];			
			return this;
		},
		setFiles : function (arrayFiles){        	
            var array;            
            for (var i=0 ;i< arrayFiles.length ;i++){            	
            	this.createBox(arrayFiles[i]);
            	this.model.attributes.files.push(arrayFiles[i]);
            }
        },
		setSignature: function (arraySignature){
			var i,
				response,
				obj=[],
				files=[];
			for (i=0;i< arraySignature.length;i++){
				if (typeof arraySignature[i] == "string"){					
					response=this.model.remoteProxyData(arraySignature[i]);
					this.createBox(response);					
					files.push(response);
											
				}else{
					this.createBox(arraySignature[i]);
					files.push(arraySignature[i]);					
				}
			}
			this.model.set("files",files);		
		},
		changeID : function (arrayNew){        	
            var array = this.model.attributes.files,
            	itemNew,
            	itemOld;           
            for (var i=0 ;i< arrayNew.length ;i++){            	
            	itemNew = arrayNew[i];
            	for (var j=0 ;j< array.length ;j++){
            		itemOld = array[j];
            		if(typeof itemOld === "string"){
            			if(itemNew["idOld"] === itemOld){
            				itemOld = itemNew["idNew"];
            			}            				
            		}
            		if(typeof itemOld === "object"){
            			if(itemNew["idOld"] === itemOld["id"]){
            				itemOld["id"] = itemNew["idNew"];
            			}	
            		}
            	}
            }
        }
	});


	PMDynaform.extendNamespace("PMDynaform.view.Signature_mobile",Signature_mobile);
}());