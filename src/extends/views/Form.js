(function(){
    var FormViewMobile = Backbone.View.extend({
        content : null,
        colsIndex: null,
        template: null,
        collection: null,
        items: null, 
        views: [],
        renderTo: document.body,
        project: null,
        initialize: function(options) {
            var defaults = {
                factory: {
                    products: {
                        "form": {
                            model: PMDynaform.model.Form,
                            view: PMDynaform.view.FormMobile
                        },
                        "fieldset": {
                            model: PMDynaform.model.Fieldset,
                            view: PMDynaform.view.Fieldset
                        },
                        "panel": {
                            model: PMDynaform.model.Panel,
                            view: PMDynaform.view.PanelMobile
                        }
                    },
                    defaultProduct: "form"
                }
            };
            if (options.renderTo) {
                this.renderTo = options.renderTo;
            }
            if(options.project) {
                this.project= options.project;
            }
            //this.renderTo = document.body;
            this.makeItem();
            this.render();
            this.afterRender(); 
        },
        getData: function () {
            var i, k, fields, panels;
            var panels = this.model.get("items");
            var formData = this.model.getData();
            for (var i = 0; i < panels.length; i+=1) {
                //var formData = this.views[i].model.getData();
                fields = this.views[i].items.asArray();
                for (var k = 0; k < fields.length; k+=1) {
                    if ((typeof fields[k].getData === "function") && 
                        (fields[k] instanceof PMDynaform.view.Field)) {
                        formData.fields.push(fields[k].getData());
                    }
                }
            }
            return formData;
        },
        makeItem: function() {
            //make panels    
            var i = 0;
            this.views = [];
            var items = this.model.get("items");
            for(i=0; i<items.length; i+=1){

                if ($.inArray(items[i].type, ["panel","form"]) >= 0) {
                    //var view = new PMDynaform.view.Panel(item);
                    var panelmodel = new PMDynaform.model.Panel(items[i]);
                    var view = new PMDynaform.view.PanelMobile({
                        model: panelmodel, 
                        project: this.project
                    });                    
                    this.views.push(view);
                }               
            }
            return this;
        },
        getPanels: function () {
            var items = (this.views.length > 0) ? this.views : [];
            return items;
        },
        render: function (){
            var i,j;
            this.$el = $(this.el);
            for(i=0; i<this.views.length; i+=1){
                this.$el.append(this.views[i].render().el);
            }
            this.$el.addClass("pmdynaform-container");
            $(this.renderTo).append(this.el);
            
        },
        afterRender: function () {
            for(var i=0; i<this.views.length; i+=1){
                this.views[i].afterRender();
            }
        }
    });
    PMDynaform.extendNamespace("PMDynaform.view.FormMobile",FormViewMobile);
    
}());
