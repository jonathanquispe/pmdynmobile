(function(){
	var FileMobile = PMDynaform.view.Field.extend({
		item: null,	
		template: _.template( $("#tpl-extfile").html()),
		templatePlus: _.template( $("#tpl-extfile-plus").html()),
		
		//templateModalBack: _.template( $("#tpl-modal-background").html()),				
		boxPlus: null,
		boxModal:null,
		boxBackground:null,
		viewsImages: [],		
		events: {
	        "click buttonImage": "onClickButton",
	        "click .pmdynaform-file-containerimage":"onClickImage"
	    },
		initialize: function () {
			//this.setOnChangeFiles();
			this.initDropArea();
		},
		initDropArea: function () {
            return this;
        },
		onDrag: function (event) {
			return this;
		},
		onDragStart: function (event) {
		},
		onDrop: function (event) {
		},		
		onClickButton: function (event) {
			var respData;			
			respData = {
					idField: this.model.get("name"),
					maxHeight : 50,
					maxWidth : 50
				};			
			if(navigator.userAgent == "formslider-android"){
				JsInterface.getPicture(JSON.stringify(respData));
				
			}
			if(navigator.userAgent == "formslider-ios"){
				this.model.attributes.project.setMemoryStack({"data":respData,"source":"IOS"});
				this.model.attributes.project.projectFlow.executeFakeIOS("activate-camera");
			}else{
				this.$el.find("input").trigger( "click" );
			}

		},
		onClickImage: function (event) {			
			var image;
			for (var i=0;i< this.viewsImages.length;i++){
				if(this.viewsImages[i].data== event.currentTarget){
					image=this.model.remoteProxyImage(this.viewsImages[i].id);
					this.createModal(image);
				}
			}
		},
		
		validateImages: function(file) {			
			if (this.model.get("preview")) {
				this.createBox(file);
			} else {
				this.createListBox(file);
			}
			
			return this;
		},
		
		createBox: function (file) {
			var rand = Math.floor((Math.random()*100000)+3);
	    	//imgName = file.name, 
	    	// not used, Irand just in case if user wanrand to print it.
	    	if(file["filePath"]){
	    		src = file["filePath"]
	    		newsrc = src;
	    	}
	    	if(file["base64"]){
	    		src = file["base64"];
	    		newsrc = this.model.makeBase64Image(src); 
	    	}	    	
	    	//src = file["thumbnails"];	    	
	    	
	    	template = document.createElement("div"),
	    	resizeImage = document.createElement("div"),
	    	preview = document.createElement("span"),
	    	progress = document.createElement("div");

	    	template.id = rand;
	    	template.className = "pmdynaform-file-containerimage";

			resizeImage.className = "pmdynaform-file-resizeimage";
			resizeImage.innerHTML = '<img class="pmdynaform-image-ext" src="'+newsrc+'"><span class="pmdynaform-file-overlay"><span class="pmdynaform-file-updone"></span></span>';	    	
	    	//console.log('<img src="'+newsrc+'"><span class="pmdynaform-file-overlay"><span class="pmdynaform-file-updone"></span></span>');
	    	preview.id = rand;
	    	preview.className = "pmdynaform-file-preview";
			preview.appendChild(resizeImage);	    	

	    	progress.id = rand;
	    	progress.className = "pmdynaform-file-progress";
	    	progress.innerHTML = "<span></span>";

	    	template.appendChild(preview);
	    	//template.appendChild(progress);
	    	template.setAttribute("data-toggle","modal");
	    	template.setAttribute("data-target","#myModal");	
	    	this.viewsImages.push({
	    		"id":file.id,				
				"data":template	    		
	    	});
	    	this.$el.find(".pmdynaform-file-droparea-ext").prepend(template);	    	
	    	return this;
		},
		createListBox: function (e, file) {
			var rand = Math.floor((Math.random()*100000)+3),
			span = document.createElement("div");
			span.innerHTML = file.name;
			this.$el.find(".pmdynaform-file-list").append(span);
			return this;
		},
		createBoxPlus: function () {
			var that = this;
			this.boxPlus=$(this.templatePlus());
			return this;
		},
		createModal: function (image) {
			var stringImage, 
				formatImage;
			formatImage = this.model.makeBase64Image(image["data"]);				
			stringImage = '<img height="auto" width="100%" src="'+formatImage+'"><span class="pmdynaform-file-overlay"><span class="pmdynaform-file-updone"></span></span>'			
			this.boxModal=this.$el.find(".modal-body");
			this.boxModal=this.$el.find(".modal-body").empty();			
			this.boxModal.append(stringImage);			
			return this;
		},
		render: function () {
			var that = this;
			this.createBoxPlus();
			this.$el.html( this.template(this.model.toJSON()) );
			if (this.model.get("hint")) {
				this.enableTooltip();
			}

			var oprand = {
				dragClass : "pmdynaform-file-active",
				dnd: this.model.get("dnd"),
			    on: {
			        load: function (e, file) {
			        	that.validate(that, e, file)
			        }
			    }
			};

			fileContainer = this.$el.find(".pmdynaform-file-droparea-ext")[0];
			this.$el.find(".pmdynaform-file-droparea-ext").append(this.boxPlus);			
			fileControl = this.$el.find("input")[0];
			if (this.model.get("dnd") || this.model.get("preview")) {
				//PMDynaform.core.FileStream.setupDrop(fileContainer, oprand); 
			}

			//PMDynaform.core.FileStream.setupIpnut(fileControl, oprand); 			
			
			return this;
		},
		
        remoteProxyData : function (arrayImages){
            var array;
            array = this.model.remoteProxyData(arrayImages);
            for (var i=0 ;i< array.length ;i++){
            	this.validateImages(array[i]);
            	this.model.attributes.images.push(array[i]);
            }
        },
        setImages : function (arrayImages){        	
            var array;            
            for (var i=0 ;i< arrayImages.length ;i++){            	
            	this.validateImages(arrayImages[i]);
            	this.model.attributes.images.push(arrayImages[i]);
            }
        }
	});

	PMDynaform.extendNamespace("PMDynaform.view.FileMobile",FileMobile);
}());