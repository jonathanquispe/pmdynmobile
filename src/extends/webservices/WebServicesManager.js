
(function(){
	var WebServicesManager = function(options) {
		this.webServices = new PMDynaform.services.WebServices();
		WebServicesManager.prototype.init.call(this, options);
	};
	
	WebServicesManager.prototype.init = function(options) {
		var defaults = {
			webServices: null			
		};
		this.viewfields = [];
		$.extend(true, defaults, options);
			
	};

	WebServicesManager.prototype.registerKey = function(key) {
		if(typeof keys === "object"){
			this.webServices.registerKey(key);
		}
		return this;	
	};

	WebServicesManager.prototype.getFullPathService = function(baseUrl , endPoint) {
		var xBase,
			xendPoint,
			xkeys,
			xFullEndPoint;
		xBase = this.webServices.getUrlBase(baseUrl);
		xendPoint = this.webServices.getEndPoint(endPoint);
		xkeys = this.webServices.keys;
		xFullEndPoint = this.getFullEndPoint(xBase+xendPoint, xkeys);
		return xFullEndPoint;	
	};

	ProjectMobile.prototype.getFullEndPoint= function (urlFormat, keys) {
        var k,
        	xUrlFormat = urlFormat;
		for (k in keys) {
			if (keys.hasOwnProperty(k)) {
				xUrlFormat = xUrlFormat.replace(new RegExp("{"+ k +"}" ,"g"), keys[k]);				
			}
		}
		return xUrlFormat;
    };


	PMDynaform.extendNamespace("PMDynaform.core.WebServicesManager", WebServicesManager);
}());