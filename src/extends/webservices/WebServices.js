
(function(){
	var WebServices = function(options) {		
		this.urlBase ={
			mobile 		 : "{server}/{workspace}/{endPointPath}",
			processmaker : "{server}/{workspace}/{endPointPath}"
		};
		this.keys ={
			server  	: "",
			processID	: "",
			taskID 		: "",
			caseID 		: "",
			workspace	: "",
			formID		: "",
			keyProject 	: "",
			typeList 	: ""
		};
		this.endPoints ={			
			jsonForm 		: "dynaform/{formID}",
			allDataCase 	: "case/{caseID}/data/all",
			submitNew 		: "process/{processID}/task/{taskID}/dynaform/{formID}",
			submitCase 		: "case/{caseID}/dynaform/{formID}/savedata",
			submitRoute 	: "case/{caseID}/dynaform/{formID}/savedata/route",			
			newTokens 		: "oauth/token",
			caseTypeList 	: "case/{caseID}/dynaform/{typeList}",
			loadDynaform 	: "case/{caseID}/dynaform/{formID}/data",
			getFormData 	: "case/{caseID}/dynaform/{formID}/data",				
			createVariable	: "process-variable",
			imageInfo		: "case/{caseID}/images/thumbnails",
			imageDownload	: "case/{caseID}/downloadfile/{imageID}",
			fileDownload	: "case/{caseID}/file/{fileID}",			
			variableList	: "process-variable",
			getImageGeo		: "image/{fileID}/thumbnails",
			variableInfo	: "process-variable/{var_uid}",			
			executeQuery 	: "process/{processID}/process-variable/{var_name}/execute-query",
			uploadFile		: ""
		};			
		WebServicesManager.prototype.init.call(this, options);
	};
	
	WebServices.prototype.init = function(options) {
		var defaults = {
			name: "",
			data: []
		};
		this.viewfields = [];
		$.extend(true, defaults, options);				
	};

	WebServices.prototype.registerEndPoint = function(alias , newEndPoint) {
		if(!this.endPoints[alias]){
			this.endPoints[alias] = newEndPoint;			
		}
		return this;
	};

	WebServices.prototype.registerUrlBase = function(alias , newUrlBase) {
		if(!this.urlBase[alias]){
			this.urlBase[alias] = newUrlBase;			
		}
		return this;
	};

	WebServices.prototype.registerKey = function(alias , newKey) {
		if(!this.keys[alias]){
			this.keys[alias] = newKey;			
		}
		return this;
	};

	WebServices.prototype.setKeys = function(newKeys) {
		if(typeof newKeys === "object" ){
			this.keys = newKeys;			
		}
		return this;
	};

	WebServices.prototype.getEndPoint = function(endPoint) {
		if(this.endPoints[endPoint]){
			return this.endPoints[endPoint];			
		}else{
			return null;	
		}		
	};

	WebServices.prototype.getUrlBase = function(urlBase) {
		if(this.urlBase[urlBase]){
			return this.urlBase[urlBase];			
		}else{
			return null;	
		}		
	};

	PMDynaform.extendNamespace("PMDynaform.services.WebServices", WebServices);
}());