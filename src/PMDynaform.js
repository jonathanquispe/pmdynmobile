"use strict";
/**
 * @class PMDynaform
 * Base class PMDynaform
 * @singleton
 */

 /**
  * @feature support for ie8
  * functions 
  */
  //.trim to support ie8
if (!Array.prototype.filter) {
  Array.prototype.filter = function(fun/*, thisArg*/) {
	'use strict';

	if (this === void 0 || this === null) {
	  throw new TypeError();
	}

	var t = Object(this);
	var len = t.length >>> 0;
	if (typeof fun !== 'function') {
	  throw new TypeError();
	}

	var res = [];
	var thisArg = arguments.length >= 2 ? arguments[1] : void 0;
	for (var i = 0; i < len; i++) {
	  if (i in t) {
		var val = t[i];

		// NOTA: Tecnicamente este Object.defineProperty deben en 
		//        el indice siguiente, como push puede ser 
		//        afectado por la propiedad en object.prototype y 
		//        Array.prototype.
		//       Pero estos metodos nuevos, y colisiones deben ser
		//       raro, así que la alternativas mas compatible.       
		if (fun.call(thisArg, val, i, t)) {
		  res.push(val);
		}
	  }
	}

	return res;
  };
}

if (!String.prototype.trim) {
  (function() {
	// Make sure we trim BOM and NBSP
	var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
	String.prototype.trim = function() {
	  return this.replace(rtrim, '');
	};
  })();
}

if (!Array.prototype.indexOf)
{
  Array.prototype.indexOf = function(elt /*, from*/)
  {
	var len = this.length >>> 0;

	var from = Number(arguments[1]) || 0;
	from = (from < 0)
		 ? Math.ceil(from)
		 : Math.floor(from);
	if (from < 0)
	  from += len;

	for (; from < len; from++)
	{
	  if (from in this &&
		  this[from] === elt)
		return from;
	}
	return -1;
  };
}

var PMDynaform = {
  VERSION: "0.1.0",
  view: {},
  model:{},
  collection:{},
  Extension: {}, 
  restData:{}
};
/**
 * Extends the PMDynaform namespace with the given `path` and making a pointer
 * from `path` to the given `class` (note that the `path`'s last token will be the pointer visible from outside
 * the definition of the class).
 *
 *      // e.g.
 *      // let's define a class inside an anonymous function
 *      // so that the global scope is not polluted
 *      (function () {
 *          var Class = function () {...};
 *
 *          // let's extend the namespace
 *          PMDynaform.extendNamespace('PMDynaform.package.Class', Class);
 *
 *      }());
 *
 *      // now PMDynaform.package.Class is a pointer to the class defined above
 *
 * @param {string} path
 * @param {Object} newClass
 * @return {Object} The argument `newClass`
 */
PMDynaform.extendNamespace = function (path, newClass) {
	var current,
		pathArray,
		extension,
		i;
	
	if (arguments.length !== 2) {
		throw new Error("Dynaform.extendNamespace(): method needs 2 arguments");
	}

	pathArray = path.split('.');
	if (pathArray[0] === 'PMDynaform') {
		pathArray = pathArray.slice(1);
	}
	current = PMDynaform;

	// create the 'path' namespace
	for (i = 0; i < pathArray.length - 1; i += 1) {
		extension = pathArray[i];
		if (typeof current[extension] === 'undefined') {
			current[extension] = {};
		}
		current = current[extension];
	}

	extension = pathArray[pathArray.length - 1];
	if (current[extension]) {
		
	}
	current[extension] = newClass;
	return newClass;
};

/**
 * Creates an object whose [[Prototype]] link points to an object's prototype (the object is gathered using the
 * argument `path` and it's the last token in the string), since `subClass` is given it will also mimic the
 * creation of the property `constructor` and a pointer to its parent called `superclass`:
 *
 *      // constructor pointer
 *      subClass.prototype.constructor === subClass       // true
 *
 *      // let's assume that superClass is the last token in the string 'path'
 *      subClass.superclass === superClass         // true
 *
 * An example of use:
 *
 *      (function () {
 *          var Class = function () {...};
 *
 *          // extending the namespace
 *          PMDynaform.extendNamespace('PMDynaform.package.Class', Class);
 *
 *      }());
 *
 *      (function () {
 *          var NewClass = function () {...};
 *
 *          // this class inherits from PMDynaform.package.Class
 *          PMDynaform.inheritFrom('PMDynaform.package.Class', NewClass);
 *
 *          // extending the namespace
 *          PMDynaform.extendNamespace('PMDynaform.package.NewClass', NewClass);
 *
 *      }());
 *
 * @param {string} path
 * @param {Object} subClass
 * @return {Object}
 */
PMDynaform.inheritFrom = function (path, subClass) {
	var current,
		extension,
		pathArray,
		i,
		prototype;

	if (arguments.length !== 2) {
		throw new Error("PMDynaform.inheritFrom(): method needs 2 arguments");
	}

	// function used to create an object whose [[Prototype]] link
	// points to `object`
	function clone(object) {
		var F = function () {};
		F.prototype = object;
		return new F();
	}

	pathArray = path.split('.');
	if (pathArray[0] === 'PMDynaform') {
		pathArray = pathArray.slice(1);
	}
	current = PMDynaform;

	// find that class the 'path' namespace
	for (i = 0; i < pathArray.length; i += 1) {
		extension = pathArray[i];
		if (typeof current[extension] === 'undefined') {
			throw new Error("PMDynaform.inheritFrom(): object " + extension + " not found, full path was " + path);
		}
		current = current[extension];
	}

	prototype = clone(current.prototype);

	prototype.constructor = subClass;
	subClass.prototype = prototype;
	subClass.superclass = current;
};

String.prototype.capitalize = function() {
	return this.toLowerCase().replace(/(^|\s)([a-z])/g, function(m, p1, p2) { return p1 + p2.toUpperCase(); });
};

jQuery.fn.extend({
	setLabel : function (newLabel) {
		var field = getFieldById(this.attr("id")) || null;
		if (typeof newLabel === "string" && field) {
			field.setLabel(newLabel);
		}
		return this;
	},
	getLabel : function () {
		var field = getFieldById(this.attr("id")) || null;
		if ( field ) {
			return field.getLabel();
		}
		return null;
  	},
	setValue : function (value) {
		var field = getFieldById(this.attr("id")) || null;
		if ( field ) {
			field.setValue(value);
		} else {
			throw new Error ("The field not exist!");
		}
		return this;
		/*validTypes = {
			number : ["integer", "float"], string : ["datetime", "string"], boolean : ["boolean"]
		},
		datatypeValue,
		dataTypeField;
		if ( $.isArray(value) || (field.getDataType() == "boolean") ) {
			datatypeValue = field.getDataType(); 
		} else {
			datatypeValue = typeof value || null;
		}
		if ( field ) {
			dataTypeField = field.getDataType();
			if (datatypeValue && validTypes[datatypeValue].indexOf(dataTypeField) > -1){
				field.setValue(value);
			} else {
				console.log("El valor no es valido, el tipo debe ser" + dataTypeField);
			}
		} else {
			return null;
		}*/
	},
	getValue : function (field) {
		var field = getFieldById(this.attr("id")) || null;
		if ( field ){
			return field.getValue();
		}
		return null;
  	},
	setOnchange : function ( handler ) {
		var item;
		if (getFieldById(this.attr("id"))){
			item = getFieldById(this.attr("id"));
		} else if ( getFormById(this.attr("id")) ) {
			item = getFormById(this.attr("id"));
		}
		if ( typeof handler === "function" && item) {
			item.setOnChange(handler);
		} else {
			throw new Error ("The id is no Valid or handler is not a function");
		}
		return this;
	},
	getInfo : function () {
		var field = getFieldById(this.attr("id")) || null;
		if (field){
			return field.getInfo();
		}
		return null;
	},
	setHref : function (value) {
		var field = getFieldById(this.attr("id")) || null;
		if (field.model.get("type") === "link") {
			field.setHref(value);
		}
		return this;
	},
	setRequired : function (field) {
		//console.log("test method field");
	},
	required : function (field) {
		//console.log("test method field");
	}
});