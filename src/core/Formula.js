(function () {
	/**
	 * @class PMDynaform.core.TokenStream
	 * Class to handle tokens or attributes for build de Formula 
	 * @param {Object} tokens
	 */
	var TokenStream = function (tokens) {
	    /**
         * @property {Number} [cursor=0] The property represents the current index 
         * of the tokens array.
         * @private
         */
	    this.cursor = 0;
	    /**
         * @property {Object} Encapsulate all tokens passed as parameter 
         * @private
         */
	    this.tokens = tokens;
	};
	 /**
     * Gets the next token element of the array
     * @return {String} element selected.
     * @private
     */
	TokenStream.prototype.next = function () {
		return this.tokens[this.cursor++];
	};
	/**
	 * The method helps in the cases when exist brackets inside of Formula
	 * @private
	 * @param {String} direction
	 * @return {String} token Element selected from token array
	 */
	TokenStream.prototype.peek = function (direction) {
		if (direction === undefined) {
            direction = 0;
        }
        return this.tokens[this.cursor + direction];
	};
	PMDynaform.extendNamespace("PMDynaform.core.TokenStream", TokenStream);


	/**
	 * @class PMDynaform.core.Tokenizer
	 * Class to manage all fields and their values, those values may be CONSTANTS, 
	 * MATH functions and FIELDS.
	 * @param {Object} tokens
	 */
	var Tokenizer = function () {
	    /**
         * @property {Number} [tokens={}] The property represents all the tokens stored
         * @private
         */
	    this.tokens = {};
	    /**
         * @property {String} [regex=null] Represents the property that encapsulate the execution
         * of the Regular Expression when the token is been finded or executed
         * @private
         */
	    this.regex = null;
	    /**
         * @property {Array} [fields=[]] Encapsulate all the fields associated or that are inside
         * of the tokens
         * @private
         */
	    this.fields = [];
	    /**
         * @property {Array} [tokenNames=[]] All the names of the tokens are stored in the array
         * @private
         */
	    this.tokenNames = [];
	    /**
         * @property {Object} [tokenFields={}] All the tokens fields are stored in this property
         * @private
         */
	    this.tokenFields = {};
	};
	/**
     * Adds new token to tokens array. If the element already exist this is replaced.
     * @param {String} name The name corresponde to name of the property inside of the tokens
     * @param {String} expression This is the value if the element is a field and is an expression
     * if the element is a bracket or some function.
     * @private
     */
	Tokenizer.prototype.addToken = function (name, expression) {
		this.tokens[name] = expression;
	};
	/**
     * Adds new expression to field
     * @param {String} name Parameter that describes to new element (Must of times is 'field')
     * @param {String} expression Value of the new element (Must of times is the name of the field)
     * @private
     */
	Tokenizer.prototype.addField = function (name, expression) {
		var expr;

		if ($.inArray(expression, this.fields) === -1) {
			this.fields.push(expression);
		}

		expr = this.fields.toString().replace(/,/g,"|");
		expr = expr.replace(new RegExp("\\[","g"),"\\[");
		expr = expr.replace(new RegExp("\\]","g"),"\\]");
		this.tokens[name] = expr;
	};
	/**
     * Sets the value for the field selected
     * @param {String} name Corresponds to name of the field
     * @param {String||Number} value Value for the field
     * @private
     */
	Tokenizer.prototype.addTokenValue = function (name, value) {
		this.tokenFields[name] = (!parseFloat(value))? 0 : parseFloat(value);
	};
	/**
     * Executes and find tokens based on the formula expression
     * @param {Object} data
     * @private
     */
	Tokenizer.prototype.tokenize = function (data) {
		var tokens;

		this.buildExpression(data);
        tokens = this.findTokens(data);
        
        return new TokenStream(tokens);
	};
	/**
     * Builds the formula expression separating by tokens 
     * @param {object} data Represent the data of the formula
     * @private
     */
	Tokenizer.prototype.buildExpression = function (data) {
		var tokenRegex = [],
		tokenName;

	    for (tokenName in this.tokens) {
	        this.tokenNames.push(tokenName);
	        tokenRegex.push('('+this.tokens[tokenName]+')');
	    }

	    this.regex = new RegExp(tokenRegex.join('|'), 'g');
	};
	/**
     * Find the tokens based of the data parameter and build an tokens array
     * @param {String} data
     * @private
     */
	Tokenizer.prototype.findTokens = function(data) {
        var tokens = [],
        match,
        group;

        while ((match = this.regex.exec(data)) !== null) {
            if (match === undefined) {
                continue;
            }

            for (group = 1; group < match.length; group+=1) {
                if (!match[group]) continue;
                
                tokens.push({
                    name: this.tokenNames[group - 1],
                    data: match[group],
                    value: null
                });
            }
        }

        return tokens;
    };
    PMDynaform.extendNamespace("PMDynaform.core.Tokenizer", Tokenizer);
	
	/**
	 * @class PMDynaform.core.Formula
	 * Class to handle all the formula property. The class support brackets that encapsulates
	  to numbers, mathematical operations and functions.
	 * @param {Object} tokens
	 */
	var Formula = function (data) {
	    this.data = data.toString();
	    this.tokenizer = new Tokenizer();
	    
	};
	/**
	 * Initializes tokens by default, like division, multiplication, constant and function.
	 * Using the {@link PMDynaform.core.Tokenizer Tokenizer} class for sets the new tokens
	 */
	Formula.prototype.initializeTokens = function () {
		this.tokenizer.addToken('whitespace', '\\s+');
	    this.tokenizer.addToken('l_paren', '\\(');
	    this.tokenizer.addToken('r_paren', '\\)');
	    this.tokenizer.addToken('float', '[0-9]+\\.[0-9]+');
	    this.tokenizer.addToken('int', '[0-9]+');
	    this.tokenizer.addToken('div', '\\/');
	    this.tokenizer.addToken('mul', '\\*');
	    this.tokenizer.addToken('add', '\\+');
	    this.tokenizer.addToken('sub', '\\-');
	    this.tokenizer.addToken('constant', 'pi|PI');
	    this.tokenizer.addToken('function', '[a-zA-Z_][a-zA-Z0-9_]*');

		return this;
	};
	/**
	 * Adds new token using the {@link PMDynaform.core.Tokenizer Tokenizer} class for
	 * set the data
	 * @param {String} name Name of the token
	 * @param {String} value Value for the new token
	 */
	Formula.prototype.addToken = function (name, value) {
		this.tokenizer.addToken(name, value);
		return this;
	};
	/**
	 * Adds new token using the {@link PMDynaform.core.Tokenizer Tokenizer} class for
	 * set the data
	 * @param {String} name Name of the token
	 * @param {String} value Value for the new token
	 */
	Formula.prototype.addField = function (name, value) {
		this.tokenizer.addField(name, value);
		return this;
	};
	/**
	 * Adds value for the field using {@link PMDynaform.core.Tokenizer Tokenizer} class for set
	 * the data
	 * @param {String} name Name of the token
	 * @param {String} value Value for the new token
	 */
	Formula.prototype.addTokenValue = function (name, value) {
		this.tokenizer.addTokenValue(name, value);
		
		return this;
	};
	/**
	 * The current method add the prefix 'Math' to data from token
	 * @param {String} token Represents the token
	 * @return {String} Return the Mathematical valid data
	 */
	Formula.prototype.consumeConstant = function(token) {
		return 'Math.' + token.data.toUpperCase();
	};
	/**
	 * Gets the valid value for the field passed as parameter
	 * @param {String} token Token that represent the data of the field
	 * @return {String} 
	 */
	Formula.prototype.consumeField = function(token) {
		return (this.tokenizer.tokenFields[token.data] === undefined) ? 0: this.tokenizer.tokenFields[token.data];
	};
	/**
	 * Adds new token using the {@link PMDynaform.core.Tokenizer Tokenizer} class
	 * @param {String} name Name of the token
	 * @param {String} value Value for the new token
	 */
	Formula.prototype.consumeFunction = function (ts, token) {
        var a = [token.data],
        t;

        while (t = ts.next()) {
            a.push(t.data);
            if (t.name === 'r_paren') {
                break;
            }
        }

        return 'Math.' + a.join('');
    };
    /**
	 * Adds new token using the {@link PMDynaform.core.Tokenizer Tokenizer} class
	 * @param {String} name Name of the token
	 * @param {String} value Value for the new token
	 */
    Formula.prototype.evaluate = function () {
        var ts,
        valueFixed,
        expr = [],
        e,
        t;

        this.initializeTokens();
        ts = this.tokenizer.tokenize(this.data);
        
        while (t = ts.next()) {
            switch (t.name) {
                case 'int':
                case 'float':
                case 'mul':
                case 'div':
                case 'sub':
                case 'add':

                    expr.push(t.data);
                    break;
                case 'field':
            		expr.push(this.consumeField(t));
                	break;	
                case 'constant':
                    expr.push(this.consumeConstant(t));
                    break;
                case 'l_paren':
                	expr.push("(");
                	break;
                case 'r_paren':
                	expr.push(")");
                	break;
                case 'function':
                    var n = ts.peek();
                    if (n && n.name === 'l_paren') {
                        expr.push(this.consumeFunction(ts, t));
                        continue;
                    }
                default:
                    break;
            }
        }

        e = expr.join('');
        
        try {
        	valueFixed = (new Function('return ' + e))();
        } catch(e) {
        	throw new Error("Error in the formula property")
        	valueFixed = 0;
        }
        return valueFixed;
    };


    PMDynaform.extendNamespace("PMDynaform.core.Formula", Formula);
}());

