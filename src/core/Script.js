(function() {
	var Script = function (options) {
		this.name = null;
		this.type = null;
		this.html = null;
		this.script = "";
		this.renderTo = document.body;

		Script.prototype.init.call(this, options);
	};
	Script.prototype.init = function (options) {
		var defaults = {
			type: "text/javascript",
			script: ""
		};

		$.extend(true, defaults, options);
		this.setType(defaults.type)
			.setScript(defaults.script);
	};
	Script.prototype.setType = function (type) {
		this.type = type
		return this;
	};
	Script.prototype.setScript = function (script) {
		this.script = script;
		return this;
	};
	Script.prototype.createHTML = function () {
		var html = document.createElement("script");		

		html.type = this.type;
		html.text = this.script
		this.html = html;

		return html;
	};
	Script.prototype.getHTML = function () {
		if (!this.html) {
			this.createHTML();
		}

		return this.html;
	};
	Script.prototype.render = function () {
		var html = this.getHTML();
		//(new Function(this.script))();
		$(this.renderTo).append(html);

		return this;
	};
    PMDynaform.extendNamespace("PMDynaform.core.Script", Script);
}());
