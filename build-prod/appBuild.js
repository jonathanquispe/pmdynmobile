/**
 * [console.log function for fake execute in IOS]
 */
/*console.log = function(log) {
  var iframe = document.createElement("IFRAME");
  iframe.setAttribute("src", "ios-log:#iOS#" + log);
  document.documentElement.appendChild(iframe);
  iframe.parentNode.removeChild(iframe);
  iframe = null;  
};*/



/**
 * Function viewNewCase
 * Description: load the form with parameters
 * (server,workspace,accesToken,refreshToken, procId, taskID, formsArrayJSON, formId, jsonForm)  
 */
var viewNewCase = function (server,workspace,accessToken,refreshToken,caseFakeID,processId, taskId, arrayForms, formID, jsonForm, dataForm){    
    var seg, mil,date=new Date();
    seg = date.getUTCSeconds();
    mil= date.getUTCMilliseconds();

    var tokens,         
        dataJson, 
        arrayForms;
	tokens= {		
		accessToken:accessToken,
		refreshToken:refreshToken
	};

    //maskLoading.showMessage();
    
    if(jsonForm){          
        if(typeof jsonForm === "string"){
           dataJson = PMURI.decode(jsonForm);
        }
        if(typeof jsonForm === "object"){                    
            dataJson = jsonForm;
        }
    }else{
    	dataJson = null;
    }
    
    if(arrayForms){
        if(typeof arrayForms === "string"){            
            arrayForms= PMURI.decode(arrayForms);                        
        }            
            //arrayForms=JSON.parse(arrayForms);
        if(typeof arrayForms === "object")                    
            arrayForms = arrayForms;
    }

    if(dataForm){
        if(typeof dataForm === "string"){                        
              dataForm = PMURI.decode(dataForm);      
        }
    }    

    window.dynaform = new PMDynaform.core.ProjectMobile({        
            server: server,
            caseFakeID:caseFakeID,
            processID: processId,
            taskID: taskId,
            workspace:workspace,
            formID:formID,
            token:tokens,
            submitRest:true,
            dynaforms : arrayForms,
            data: dataJson,
            dataForm:dataForm,
            onLine:true
        }); 

	window.dynaform.loadNewCase(); 
    date = new Date();
    seg= date.getUTCSeconds() -seg;
    mil = date.getUTCMilliseconds()-mil;
     if(mil < 0){
      seg--;
      mil=mil+1000;   
    }
    console.log("--"+seg+":"+mil+"--");  
     
};

var viewCase = function (server,workspace,accessToken,refreshToken,processId,taskId,caseId, typeList, formsArray, formID, jsonForm, dataForm){  
	var seg, mil,date=new Date();
    seg = date.getUTCSeconds();
    mil= date.getUTCMilliseconds();

    var jsonf=null;   
	tokens= {		
		accessToken:accessToken,
		refreshToken:refreshToken
	};

    if(jsonForm){          
        if(typeof jsonForm === "string"){                                      
            data = PMURI.decode(jsonForm);
        }
        if(typeof jsonForm === "object"){                    
            data = jsonForm;
        }
    }else{
        data = null;
    }
    
    if(formsArray){
        if(typeof formsArray === "string"){            
            arrayForms=PMURI.decode(formsArray);  
        }            
        if(typeof formsArray === "object")                    
            arrayForms = formsArray;
    } 

     if(dataForm){
        if(typeof dataForm === "string"){            
            dataForm = PMURI.decode(dataForm);    
        }                   
    }    
    
    window.dynaform = new PMDynaform.core.ProjectMobile({        
            server: server,
            workspace:workspace,            
            token:tokens,
            processID: processId,            
            caseID: caseId,
            taskID: taskId,
            typeList:typeList,
            dynaforms: arrayForms,
            formID:formID,
            data:data,
            dataForm:dataForm,
            submitRest:true 
        });    	
    window.dynaform.loadCase(); 
     date = new Date();
    seg= date.getUTCSeconds() -seg;
    mil = date.getUTCMilliseconds()-mil;
    if(mil < 0){
      seg--;
      mil=mil+1000;   
    }
    console.log("--"+seg+":"+mil+"--");           
};

var loadToolbar = function() {
	var div = document.createElement("div");
	div.style.cssText = "float:right; margin:1%;";
	div.innerHTML = '<button type="button" onclick="previousSample();" class="btn btn-info">previous form</button>  <button type="button" onclick="nextSample();return false;" class="btn btn-info">next form</button>';
	$(document.body).prepend(div);

};

/**
 * [adjustHeight description]
 * @return {[type]} [description] RFC
 */
var adjustHeight = function() {
    var windowHeight, 
        containerHeight;
    windowHeight= $(window).height();
    $("#container")[0].style.height= "auto";
    containerHeight = $("#container").height();
    if(containerHeight+50 < windowHeight){
        $("#container").height(windowHeight);
    }     
};

/*var maskLoading = {
    msgTpl : _.template($('#tpl-loading').html()),
    showMessage: function (){
        $('body').append(this.msgTpl({
            title : "Loading",
            msg: "Please wait while the data is loading..."
        }));
        //$('body').find("#shadow-form").css("height",this.view.$el.height()+"px");
    },
    hideMessage:function (){
        $('body').find(".pmdynaform-form-message-loading").remove();
        $("#shadow-form").remove();
    }
};*/

var kitKatMode = null;
var setKitKatMode = function(value) {
    kitKatMode = value;     
};


var PMURI ={
    encode:function (input){
               
    },
    decode:function (input){
        input=JSON.parse(input, function (key, value){
            if(typeof value === "string"){
                value=value.replace(/(&d\|t&)/g, "\n");         
                value=value.replace(/(&c\|t&)/g, "'");         
                value=value.replace(/(&t\|t&)/g, "\t");    
            }            
            return value;
        });                         
        return input; 
    }
};

var Base64 = {

    // private property
    _keyStr : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",

    // public method for encoding
    encode : function (input) {
        var output = "";
        var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
        var i = 0;

        input = Base64._utf8_encode(input);

        while (i < input.length) {

            chr1 = input.charCodeAt(i++);
            chr2 = input.charCodeAt(i++);
            chr3 = input.charCodeAt(i++);

            enc1 = chr1 >> 2;
            enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
            enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
            enc4 = chr3 & 63;

            if (isNaN(chr2)) {
                enc3 = enc4 = 64;
            } else if (isNaN(chr3)) {
                enc4 = 64;
            }

            output = output +
            this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) +
            this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);

        }

        return output;
    },

    // public method for decoding
    decode : function (input) {
        var output = "";
        var chr1, chr2, chr3;
        var enc1, enc2, enc3, enc4;
        var i = 0;

        input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

        while (i < input.length) {

            enc1 = this._keyStr.indexOf(input.charAt(i++));
            enc2 = this._keyStr.indexOf(input.charAt(i++));
            enc3 = this._keyStr.indexOf(input.charAt(i++));
            enc4 = this._keyStr.indexOf(input.charAt(i++));

            chr1 = (enc1 << 2) | (enc2 >> 4);
            chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
            chr3 = ((enc3 & 3) << 6) | enc4;

            output = output + String.fromCharCode(chr1);

            if (enc3 != 64) {
                output = output + String.fromCharCode(chr2);
            }
            if (enc4 != 64) {
                output = output + String.fromCharCode(chr3);
            }

        }

        output = Base64._utf8_decode(output);

        return output;

    },

    // private method for UTF-8 encoding
    _utf8_encode : function (string) {
        string = string.replace(/\r\n/g,"\n");
        var utftext = "";

        for (var n = 0; n < string.length; n++) {

            var c = string.charCodeAt(n);

            if (c < 128) {
                utftext += String.fromCharCode(c);
            }
            else if((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            }
            else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }

        }

        return utftext;
    },

    // private method for UTF-8 decoding
    _utf8_decode : function (utftext) {
        var string = "";
        var i = 0;
        var c = c1 = c2 = 0;

        while ( i < utftext.length ) {

            c = utftext.charCodeAt(i);

            if (c < 128) {
                string += String.fromCharCode(c);
                i++;
            }
            else if((c > 191) && (c < 224)) {
                c2 = utftext.charCodeAt(i+1);
                string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                i += 2;
            }
            else {
                c2 = utftext.charCodeAt(i+1);
                c3 = utftext.charCodeAt(i+2);
                string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                i += 3;
            }
        }
        return string;
    }
}