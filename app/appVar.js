// http://coenraets.org/blog/2013/06/building-modular-web-applications-with-backbone-js-and-requirejs-sample-app/

// var data;  
//$.getJSON("data/layout.json", function(data) {
//	loadForm(data);
//}).done(function() {
//    console.log( "success" );
//  })
//  .fail(function(a) {
//    console.error( "Error loading fields" );
//  });

var loadForm = function (data) {
	var fieldCollection, fieldView,variables;
	if (typeof data !== "object") {
		data = JSON.parse(data);
	}
	$.getJSON("data/variables.json", function(data) {
		variables= data;		
	});
	//fieldCollection = new FieldCollection(data.fields);
	/*	
	fieldView = new PanelFormView({ 
		tagName: "form",
		renderTo: $('.container'),
		collection:  fieldCollection
	});*/
	PMDynaform.variables = variables;
	
	AppModel = new PMDynaform.Model.Form(data);
	AppView = new PMDynaform.View.Form({
		tagName: "div",
		renderTo: $('.container'),
		model: AppModel
	});
	//FormView = new PMDynaform.View.Form(data);
	//The render method is applied inside of the method
};

$(document).ready(function () {
    var data = document.location.search.replace("?data=", "");
    if (data !== "") {
        data = decodeURIComponent(data);
        loadForm(data);
    } else {
        $.getJSON("data/layout.json", function (data) {
            loadForm(data);
        }).done(function () {
            console.log("success");
        })
                .fail(function (a) {
            console.error("Error loading fields");
        });
    }

});